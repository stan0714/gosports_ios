//
//  eventViewCell.h
//  TennisLife
//
//  Created by Stanley Liu on 10/1/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface joinTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *subLabel;
@property (nonatomic, weak) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;


@end
