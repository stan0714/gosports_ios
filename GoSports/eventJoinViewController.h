//
//  eventJoinViewController.h
//  TennisLife
//
//  Created by Stanley Liu on 10/15/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eventJoinViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak) NSString *eventId;
@property (weak, nonatomic) IBOutlet UILabel *eventIdLabel;


@end
