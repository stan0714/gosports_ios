//
//  newEventViewController.h
//  GoSports
//
//  Created by Stanley Liu on 9/17/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newEventViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate,UIAlertViewDelegate,UITextFieldDelegate>;
@property (weak, nonatomic) IBOutlet UITextField *timeFeild;
@property (weak, nonatomic) IBOutlet UITextField *endTimeFeild;

@property (weak, nonatomic) IBOutlet UITextField *ntrText;
@property (weak, nonatomic) IBOutlet UITextField *cityText;
@property (weak, nonatomic) IBOutlet UITextField *locationText;
@property (weak, nonatomic) IBOutlet UITextView *remark;
@property (weak, nonatomic) IBOutlet UITextField *typeText;
@property (weak, nonatomic) IBOutlet UISwitch *privat;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
- (IBAction)submitAction:(id)sender;



@end
