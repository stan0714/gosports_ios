//
//  joinMemberViewController.m
//  TennisLife
//
//  Created by Stanley Liu on 10/24/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "joinMemberViewController.h"
#import "ToolUtil.h"
#import "gosportsAppDelegate.h"
#import "SQLLiteUtil.h"

@interface joinMemberViewController (){
    gosportsAppDelegate *appDelegate;
    UIPickerView *ntrPicker,*cityPicker,*locationPicker,*typePicker,*sexPicker ,*userHandPicker;
    UIToolbar *mypickerToolbar;
    NSArray *cityArray,*locationArray;
    NSArray *array,*arrState,*sexArray,*userHandArray;
    NSDictionary *userInfoResult;
    
}

@end

@implementation joinMemberViewController
- (IBAction)saveAction:(id)sender {
    
    
}

- (void)viewDidLoad {
    //取得event清單

    [super viewDidLoad];
    //取得LOCATION清單
    [[ToolUtil shared] getLocationInfo];
    [self initView];
    [self initArray];
    [self initcityPick];
    [self initntrPick];
    [self initSexPicker];
    [self initUseHandPicker];
    //初始化Scrollview
    //[self.scrollview setContentSize:CGSizeMake(_scrollview.frame.size.width, 2*_scrollview.frame.size.height)];
    NSLog(@"_scrollview.frame.size %f",_scrollview.frame.size.height);
    [self.scrollview setContentSize:CGSizeMake(_scrollview.frame.size.width, 1500)];
    [self.scrollview setScrollEnabled:YES];

    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    array=nil;
    arrState=nil;
    sexArray=nil;
    ntrPicker=nil;
    cityPicker=nil;
    locationPicker=nil;
    typePicker=nil;
    mypickerToolbar=nil;
    cityArray=nil;
    locationArray=nil;
    appDelegate=nil;
    _ntrField=nil;
    _cityField=nil;
    _locationField=nil;
    _remarkText=nil;
    userHandPicker=nil;
    sexPicker=nil;



}

-(void) initArray{
    
    if (appDelegate.cityArray == nil || [appDelegate.cityArray count] == 0) {
        NSString *citySql=@"select distinct(city) from  location";
        
        arrState=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"city"];
        appDelegate.cityArray =arrState;
        
    }else{
        arrState=appDelegate.cityArray;
    }
    
    
    
    array = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9" ,nil];
    //男生是1 女生是0
    sexArray=[[NSArray alloc] initWithObjects:@"女",@"男" ,nil];
    //右是1 左是0
    userHandArray=[[NSArray alloc] initWithObjects:@"左",@"右" ,nil];

    //cityArray = [[NSArray alloc] initWithObjects:@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19" ,nil];
    locationArray = [[NSArray alloc] initWithObjects:@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29" ,nil];
    //typeArray=[[NSArray alloc] initWithObjects:@"練球",@"競賽" ,nil];
}

-(void) initView{
    appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
    NSString *serverResponse=[[ToolUtil shared] getUserInfo:appDelegate.userId useToken:@""];
    NSDictionary *repsonse=[[ToolUtil shared] getJsonNSDictionary:serverResponse];
    NSLog(@"1:%@",repsonse);
    if([repsonse[@"status"] isEqualToString:@"-100"]){
        userInfoResult = [repsonse objectForKey:@"USER"];
        //dict=[[ToolUtil shared] getJsonNSDictionary:result];
        NSLog(@"2:%@",userInfoResult[@"email"]);

        _nameLabel.text=appDelegate.userName;
        //球拍
        if(![userInfoResult[@"racket"] isKindOfClass:[NSNull class]]){
            _racketField.text=userInfoResult[@"racket"];
        }
        if(![userInfoResult[@"sex"] isKindOfClass:[NSNull class]]){
            NSLog(@"compare sex %@",userInfoResult[@"sex"]);
            if([userInfoResult[@"sex"] intValue] == 0){
                 NSLog(@"compare sex %@",@"女");
               self.sexField.text=@"女";
            }else{
                self.sexField.text=@"男";
                NSLog(@"compare sex %@",@"男");

            }

        }
        //ＮＴＲ等級
        if(![userInfoResult[@"level"] isKindOfClass:[NSNull class]]){
            _ntrField.text=userInfoResult[@"level"];
        }
        //age
        if(![userInfoResult[@"age"] isKindOfClass:[NSNull class]]){
            _ageFireld.text=userInfoResult[@"age"];
        }
        //冠用手
        if(![userInfoResult[@"useHand"] isKindOfClass:[NSNull class]]){
             _useHandField.text=userInfoResult[@"useHand"];
        }
        //favLocation
        if(![userInfoResult[@"favLocation"] isKindOfClass:[NSNull class]]){
            _locationField.text=userInfoResult[@"favLocation"];
        }
        
        
    }else{

    }

    

}


-(void) initUseHandPicker{
    
    //self.sexField.text=@"請選擇城市";
    
    userHandPicker= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    userHandPicker.delegate = self;
    
    userHandPicker.dataSource = self;
    
    userHandPicker.tag=6;
    
    [userHandPicker  setShowsSelectionIndicator:YES];
    
    self.useHandField.inputView =  userHandPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(useHandPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.useHandField.inputAccessoryView = mypickerToolbar;
    
}



-(void)useHandPickerDoneClicked

{
    
    //NSLog(@"Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [userHandArray objectAtIndex:[userHandPicker selectedRowInComponent:0]];
        
        self.useHandField.text = stateSelect;
        
    }
    
}



-(void) initSexPicker{
    
    //self.sexField.text=@"請選擇城市";

    sexPicker= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    sexPicker.delegate = self;
    
    sexPicker.dataSource = self;
    
    sexPicker.tag=5;
    
    [sexPicker  setShowsSelectionIndicator:YES];
    
    self.sexField.inputView =  sexPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(sexPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.sexField.inputAccessoryView = mypickerToolbar;
    
}



-(void)sexPickerDoneClicked

{
    
    //NSLog(@"Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [sexArray objectAtIndex:[sexPicker selectedRowInComponent:0]];
    
        self.sexField.text = stateSelect;
        
    }
    
}




-(void) initcityPick{
    
//    self.cityField.text=@"請選擇城市";
    
    cityPicker= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    cityPicker.delegate = self;
    
    cityPicker.dataSource = self;
    
    cityPicker.tag=4;
    
    [cityPicker  setShowsSelectionIndicator:YES];
    
    self.cityField.inputView =  cityPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(cityPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.cityField.inputAccessoryView = mypickerToolbar;
    
}



-(void)cityPickerDoneClicked

{
    
    //NSLog(@"Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [arrState objectAtIndex:[cityPicker selectedRowInComponent:0]];
        
        
        self.cityField.text = stateSelect;
        
        NSString *citySql=@"select name from  location where city=:city";
        
        NSDictionary *locDictionary = [NSDictionary dictionaryWithObjectsAndKeys:stateSelect,@"city", nil];
        locationArray=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"name" withDic:locDictionary];
        [self.locationField setEnabled:YES];
        [self initlocationPick];
        
    }
    
}



-(void) initlocationPick{
    
    self.locationField.text=[locationArray objectAtIndex:0];
    //    arrState = [[NSArray alloc] initWithObjects:@"NewYork",@"NewJercy",@"Carlifornia",@"Florida",@"Fremont",@"SantaClara",@"San Diego",@"San Fransisco",@"San Jose",nil];
    
    
    locationPicker= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    locationPicker.delegate = self;
    
    locationPicker.dataSource = self;
    
    locationPicker.tag=2;
    
    [locationPicker  setShowsSelectionIndicator:YES];
    
    self.locationField.inputView =  locationPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(locationPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.locationField.inputAccessoryView = mypickerToolbar;
    
}

-(void)locationPickerDoneClicked

{
    
    //NSLog(@"Location Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [locationArray objectAtIndex:[locationPicker selectedRowInComponent:0]];
        
        
        self.locationField.text = stateSelect;
    }
    
}







-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    //NSLog(@"pickerView numberOfRowsInComponent pickerView.tag %ld",(long)pickerView.tag);
    
    if (pickerView.tag == 3){
        return [array count];
    }
    else if(pickerView.tag == 1){
        return [cityArray count];
    }else if (pickerView.tag==2){
        return [locationArray count];
    }else if (pickerView
              .tag==4){
        return [arrState count];
    }else if (pickerView
              .tag==5){
        return [sexArray count];
    }else if (pickerView
              .tag==6){
        return [userHandArray count];
    }else{
        return [userHandArray count];
        
    }
    
    //return [array count];
}
-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSLog(@"pickerView titleForRow pickerView.tag %ld",(long)pickerView.tag);
    if (pickerView.tag == 3){
        return [array objectAtIndex:row];
    }
    else if(pickerView.tag == 1){
        return [cityArray objectAtIndex:row];
    }else if (pickerView.tag==2){
        return [locationArray objectAtIndex:row];
        
    }else if(pickerView.tag==4){
        return [ arrState objectAtIndex:row];
      
    }else if(pickerView.tag==5){
        return [ sexArray objectAtIndex:row];
    }else if(pickerView.tag==6){
        return [userHandArray objectAtIndex:row];
    }else{
        return [ userHandArray objectAtIndex:row];
    }
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    NSLog(@"pickerView didSelectRow pickerView.tag %ld",(long)pickerView.tag);
    
    if (pickerView.tag == 3){
        NSString *ntv  = [array objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"ntv: %@",ntv);
    }
    else if(pickerView.tag == 1){
        NSString *city  = [cityArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"city: %@",city);
    }else if(pickerView.tag == 2){
        NSString *location  = [locationArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"location: %@",location);
    }else if(pickerView.tag==4){
        NSString *arrStat   = [arrState objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"arrState: %@",arrStat);
    }else if(pickerView.tag==5){
        NSString *arrStat   = [sexArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"arrState: %@",arrStat);
    }else if(pickerView.tag==6){
        NSString *arrStat   = [userHandArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"arrState: %@",arrStat);
    }else{
        NSString *typeStat   = [userHandArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"type: %@",typeStat);
    }
    
}


-(void) initntrPick{
    
//    self.ntrField.text=@"請選擇等級";
    //    arrState = [[NSArray alloc] initWithObjects:@"NewYork",@"NewJercy",@"Carlifornia",@"Florida",@"Fremont",@"SantaClara",@"San Diego",@"San Fransisco",@"San Jose",nil];
    
    
    ntrPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    ntrPicker.delegate = self;
    
    ntrPicker.dataSource = self;
    
    ntrPicker.tag=3;
    
    
    [ntrPicker  setShowsSelectionIndicator:YES];
    
    self.ntrField.inputView =  ntrPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(ntrPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.ntrField.inputAccessoryView = mypickerToolbar;
    
}

-(void)ntrPickerDoneClicked

{
    
    //NSLog(@"NTR Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [array objectAtIndex:[ntrPicker selectedRowInComponent:0]];
        
        
        self.ntrField.text = stateSelect;
    }
    
}



 /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
