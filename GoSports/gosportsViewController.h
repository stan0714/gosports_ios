//
//  gosportsViewController.h
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface gosportsViewController : UIViewController

@property NSArray *books;

@property (weak, nonatomic) IBOutlet UIImageView *image;
//@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
//
//@property (weak, nonatomic) IBOutlet UITabBarItem *LoginBarItem;
//@property (weak, nonatomic) IBOutlet UITabBarItem *RegisterBarItem;
@property (weak, nonatomic) IBOutlet UIButton *LoginButton;
@property (weak, nonatomic) IBOutlet UIButton *SignInButton;

@end
