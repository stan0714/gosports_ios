//
//  eventJoinViewController.m
//  TennisLife
//
//  Created by Stanley Liu on 10/15/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "eventJoinViewController.h"
#import "gosportsAppDelegate.h"
#import "ToolUtil.h"
#import "joinTableCell.h"
@interface eventJoinViewController (){
    gosportsAppDelegate *appDelegate;
    NSArray *memberData;
}

@end

@implementation eventJoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
    [self initView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [memberData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //製作可重複利用的表格欄位Cell
//    static NSString *CellIdentifier = @"joinTableCell";
    
//    static NSString *simpleTableIdentifier = @"joinTableCell";
    

    joinTableCell *cell =[tableView dequeueReusableCellWithIdentifier:@"joinTableCell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"joinTableCell" bundle:nil] forCellReuseIdentifier:@"joinTableCell"];

        cell = [tableView dequeueReusableCellWithIdentifier:@"joinTableCell"];
        //cell.nameLabel = dic[@"USER_ID"];
    }
//
//    
//    cell.nameLabel.text = dic[@"USER_ID"];
//    cell.image.image=[UIImage imageNamed:@"AppIcon50x50"];
//    cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
//    cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
//
    
    NSDictionary *dic=[memberData objectAtIndex:indexPath.row];
    cell.nameLabel.text = dic[@"USER_ID"];
    cell.image.image=[UIImage imageNamed:@"AppIcon50x50"];
    cell.agreeButton.tag=indexPath.row;
    [cell.agreeButton addTarget:self
                         action:@selector(agreeEvent:)
               forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(joinTableCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic=[memberData objectAtIndex:indexPath.row];
    cell.nameLabel.text = dic[@"USER_ID"];
    cell.image.image=[UIImage imageNamed:@"AppIcon50x50"];
    
    //設定欄位的內容與類型
    UIButton *agreeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    agreeButton.frame = CGRectMake(300.0f, 6.0f, 75.0f, 30.0f);
    agreeButton.tag=indexPath.row;
    [agreeButton setTitle:@"Agree" forState:UIControlStateNormal];
    
    agreeButton.tag=indexPath.row;
    [agreeButton addTarget:self
                        action:@selector(agreeEvent:)
              forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:agreeButton];
    
}



- (IBAction)agreeEvent:(UIButton *)sender
{
    //    if (sender.tag == 0)
    //    {
    
//    NSString *locatonName=[[ToolUtil shared]getLocationName:[locationArray objectAtIndex:sender.tag]];
    
    NSLog(@"JoinEvent %ld",(long)sender.tag);
//    //[self showJoinAlert:[eventIDArray objectAtIndex:sender.tag] with];
//    [self showJoinAlert:[eventIDArray objectAtIndex:sender.tag] locationName:locatonName];
    //    }
    //    NSLog(@"JoinEvent %@",sender);
}


-(void)initView{
    if([_eventId length]>0){
        _eventIdLabel.text=@"Other Function";
    }else{
        NSString *eventId=[[NSUserDefaults standardUserDefaults] stringForKey:@"eventId"];
        _eventIdLabel.text=eventId;
        NSString *response=[[ToolUtil shared]getEventList:eventId userId:@"" withLevel:@""];
                NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
        if(![dict[@"status"] isEqualToString:@"-100" ]){

        }else{
            dict=[[ToolUtil shared] getJsonNSDictionary:dict[@"event"]];
            //將Dictory 轉成Array
            NSArray *members = [dict valueForKeyPath:@"MEMBERS"];
            NSLog(@"Array:%@",members);
            
            
//                            NSDictionary *dic=[[ToolUtil shared] getJsonNSDictionary:result[0]];
            
            //NSArray *dic2= MEMBERS[0];
            memberData=members[0];
           NSLog(@"dic:%@ :",memberData);
            NSLog(@"----------");
            NSLog(@"dic2:%@  :",memberData[0]);
            NSDictionary *dic3=memberData[0];
            NSLog(@"dic3:%@  :",dic3[@"USER_ID"]);
            
        }

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath      *)indexPath;
{

        return 44;

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    memberData=nil;
    appDelegate=nil;
    _eventIdLabel=nil;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
