//
//  NSObject+Annotation.m
//  TennisLife
//
//  Created by Stanley Liu on 10/6/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "Annotation.h"
#import <MapKit/MapKit.h>

@implementation Annotation
    
    @synthesize coordinate;
    @synthesize title;
    @synthesize subtitle;
    
    -(id) initWithCoordinate: (CLLocationCoordinate2D) the_coordinate
    {
        if (self = [super init])
        {
            coordinate = the_coordinate;
        }
        return self;
    }
    

    
@end

