//
//  registerController.h
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface registerController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backbutton;

@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UITextField *accountFiled;
@property (weak, nonatomic) IBOutlet UITextField *pwdField;
@property (weak, nonatomic) IBOutlet UITextField *confirmpwdField;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)regiserAction:(id)sender;

@end
