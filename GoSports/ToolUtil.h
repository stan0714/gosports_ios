//
//  ToolUtil.h
//  myplay1_sample_code
//
//  Created by Stanley Liu on 8/19/14.
//  Copyright (c) 2014 TWM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
@interface ToolUtil : NSObject

+ (ToolUtil *)shared;

- (NSString*)getNowTime;
- (NSString*)getNowTime:(NSString *) format;
- (NSString*)MD5:(NSString *) input;
- (NSDictionary *)getJsonNSDictionary:(NSString *) input;
- (void) getLocationInfo;
- (void) getEventList;
- (NSString *) getEventList:(NSString *) eventId userId:(NSString *) userId withLevel:(NSString *) level;
- (void) registerToken;
-(BOOL) compareTime :(NSString *) dateformat currentTime:(NSString *) time1 compareTime:(NSString *) time2;
-(NSString*)changeDateFormt:(NSString *) oldformat newformat:(NSString *) newformat dateString:(NSString *) dateString;
-(NSString *)getLocationName:(NSString *) id;
-(NSString *)getShowEventMsg:(NSString *) id withLocationName:(NSString *)locationName;
-(NSString *)joinEvent:(NSString *) eventId;
-(NSInteger *)numberPushEvent:(NSString *) method;
-(NSString *) getUserInfo :(NSString *) userId useToken:(NSString *) token;

@end;

