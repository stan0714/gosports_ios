//
//  joinMemberViewController.h
//  TennisLife
//
//  Created by Stanley Liu on 10/24/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface joinMemberViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate,UIAlertViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
-(void) initlocationPick;
@property (weak, nonatomic) IBOutlet UITextView *remarkText;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@property (weak, nonatomic) IBOutlet UITextField *ntrField;
@property (weak, nonatomic) IBOutlet UITextField *sexField;
@property (weak, nonatomic) IBOutlet UITextField *ageFireld;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UITextField *useHandField;
@property (weak, nonatomic) IBOutlet UITextField *racketField;

@end
