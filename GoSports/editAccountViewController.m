//
//  editAccountViewController.m
//  TennisLife
//
//  Created by Stanley Liu on 11/5/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "editAccountViewController.h"
#import "ToolUtil.h"
#import "gosportsAppDelegate.h"

@interface editAccountViewController (){
    gosportsAppDelegate *appDelegate;
    NSDictionary *dict;
}

@end

@implementation editAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
    NSString *serverResponse=[[ToolUtil shared] getUserInfo:appDelegate.userId useToken:@""];
    NSDictionary *repsonse=[[ToolUtil shared] getJsonNSDictionary:serverResponse];
    NSLog(@"1:%@",repsonse);
    if([repsonse[@"status"] isEqualToString:@"-100"]){
         NSDictionary *result = [repsonse objectForKey:@"USER"];
        //dict=[[ToolUtil shared] getJsonNSDictionary:result];
        NSLog(@"2:%@",result[@"email"]);
        //Convert Json to NSArray
//        NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding]options:0 error:NULL];
        
//        for (id object in result) {
//            NSLog(@"2:%@",result[object]);
//            // do something with object
//        }

////        
//
//
        _idLabel.text=appDelegate.userName;
        if(![result[@"email"] isKindOfClass:[NSNull class]]){
            _emailField.text=result[@"email"];
        }
        if(![result[@"mobile"] isKindOfClass:[NSNull class]]){
            _phoneField.text=result[@"mobile"];
        }

    }else{
//        dict=[[ToolUtil shared] getJsonNSDictionary:userInfo[@""]];
//        _idLabel.text=appDelegate.userName;
//        _emailField.text=dict[@"email"];
//        _phoneField.text=dict[@"mobile"];
    }

    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    appDelegate=nil;
    dict=nil;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
