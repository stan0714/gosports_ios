//
//  GetData.m
//  package
//
//  Created by Stanley on 14/8/17.
//  Copyright (c) 2014年 kevin. All rights reserved.
//
//

#import "ToolUtil.h"
#import "GetData.h"
#import "gosportsAppDelegate.h"
#import "LocationObject.h"
#import "SQLLiteUtil.h"


@implementation ToolUtil

static ToolUtil *_shared = nil;
+ (ToolUtil *)shared; {
	if (_shared == nil) {
		_shared = [self new];
	}
	return _shared;
}

- (NSString*)getNowTime{
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *time=[dateFormatter stringFromDate:now];
    NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
    return time;
    

}


- (NSString*)getNowTime:(NSString *) format
{
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *time=[dateFormatter stringFromDate:now];
    NSLog(@"The Current Time is %@",[dateFormatter stringFromDate:now]);
    return time;
    
    
}


-(NSString*)changeDateFormt:(NSString *) oldformat newformat:(NSString *) newformat dateString:(NSString *) dateString{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:oldformat];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:newformat];
    return [formatter stringFromDate:myDate];
}

- (NSString*)MD5:(NSString *) input
{
    // Create pointer to the string as UTF8
    const char *ptr = [input UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer

    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}
-(NSDictionary *)getJsonNSDictionary:(NSString *) input
{

NSData *jsonData = [input dataUsingEncoding:NSUTF8StringEncoding];
NSError *e;
return [NSJSONSerialization JSONObjectWithData:jsonData options:nil error:&e];
    
}

- (void) getLocationInfo{

   //NSString *dateFormat=@"YYYY-MM-dd";
   NSString *location_time= [[NSUserDefaults standardUserDefaults] stringForKey:@"location_time"];
    NSLog(@"%lu %@",(unsigned long)[location_time length],location_time);
    
   
    
    
    BOOL gate=NO;
    

    if(!gate){
    
    gosportsAppDelegate *appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
    NSDictionary *_propertyInfo=appDelegate.propertyInfo;
    
    NSMutableDictionary *register_dir=[[NSMutableDictionary alloc] init];
    [register_dir setObject:@"" forKey:@"city"];
    [register_dir setObject:@"" forKey:@"town"];

    
    NSString *para=[[GetData shared] encodeFormPostParametersWithJson:register_dir];
    // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
    NSLog(@"para:%@ ",para);
    
    NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"location_path"]];
    
    NSLog(@"web_url:%@ ",web_url);
    
    NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
    
    NSLog(@"response: %@ ",response);
        
        if([response length]>0){
       [[NSUserDefaults standardUserDefaults] setObject: [self getNowTime:@"YYYY-MM-dd"] forKey:@"location_time"];
        
        NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
        
        if(![dict[@"status"] isEqualToString:@"-100" ]){
            NSLog(@"not success");
        }else{

            
            
            NSString *locationSql=@"CREATE TABLE  IF NOT EXISTS location (seq number PRIMARY KEY, name text,address text,city text,town text,location text,clay number,grass number,latitude number,longitude number,indoor number,hard number)";
            
            NSLog(@"success");
            [[SQLLiteUtil shared]createTable:locationSql];
            //取出球場資訊
            NSString *result = [dict objectForKey:@"result"];
            
            NSLog(@"%@",result);
            
            
            //Convert Json to NSArray
            NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding]options:0 error:NULL];
            
            NSLog(@"jsonObject=%@", jsonObject);
            
            for ( NSDictionary *loca in jsonObject )
            {
//                NSLog(@"----");
//                 NSLog(@"seq: %@", loca[@"seq"] );
//                NSLog(@"name: %@", loca[@"name"] );
//                 NSLog(@"city: %@", loca[@"city"] );
//                NSLog(@"town: %@", loca[@"town"] );
//                NSLog(@"location: %@", loca[@"address"] );
//                NSLog(@"clay: %@", loca[@"clay"] );
//                NSLog(@"grass: %@", loca[@"grass"] );
//                NSLog(@"hard: %@", loca[@"hard"] );
//                NSLog(@"latitude: %@", loca[@"latitude"] );
//                NSLog(@"longitude: %@", loca[@"longitude"] );
//                NSLog(@"indoor: %@", loca[@"indoor"] );
//                NSLog(@"----");

                
                NSString *sql = @"insert or replace  into location (seq, name,city,town,clay,grass,latitude,longitude,indoor,address,hard) values (:seq, :name,:city,:town,:clay,:grass,:latitude,:longitude,:indoor,:address,:hard)";
//                NSLog(@"%@",sql);
                [[SQLLiteUtil shared]insertDataToDB:sql withDic:loca];
                

                

                
            }
            
        }

        
        
    }else{
        //NSLog(@"Remove Object");
        //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"location_time"];
    }
        //NSLog(@"Server has some problem!!");
    }
    
}


- (void) getEventList{
    
//    NSString *dateFormat=@"YYYY-MM-dd";
//    NSString *location_time= [[NSUserDefaults standardUserDefaults] stringForKey:@"location_time"];
//    NSLog(@"%lu %@",(unsigned long)[location_time length],location_time);
    BOOL gate=NO;
    
//    if(![location_time length]==0){
//        NSLog(@"compare time");
//        gate=[self compareTime:dateFormat : location_time:[self getNowTime:dateFormat]];
//    }
    
    if(!gate){
        
        gosportsAppDelegate *appDelegate = (gosportsAppDelegate *)[[UIApplication sharedApplication]delegate];
        NSDictionary *_propertyInfo=appDelegate.propertyInfo;
        
        NSMutableDictionary *query_Event=[[NSMutableDictionary alloc] init];
        [query_Event setObject:@"" forKey:@"userId"];
        [query_Event setObject:@"" forKey:@"level"];
        [query_Event setObject:@"" forKey:@"eventId"];
        [query_Event setObject:@"" forKey:@"locationId"];
        [query_Event setObject:[NSNumber numberWithInt:1] forKey:@"page"];
        [query_Event setObject:[NSNumber numberWithInt:10] forKey:@"row"];
        [query_Event setObject:@"" forKey:@"token"];
        
        NSString *para=[[GetData shared] encodeFormPostParametersWithJson:query_Event];
        // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
        NSLog(@"para:%@ ",para);
        
        NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"query_event_path"]];
        
        NSLog(@"web_url:%@ ",web_url);
        
        NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
        
        NSLog(@"response: %@ ",response);
//        [[NSUserDefaults standardUserDefaults] setObject: [self getNowTime:@"YYYY-MM-dd"] forKey:@"location_time"];
        
        NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
        
        if(![dict[@"status"] isEqualToString:@"-100" ]){
            NSLog(@"not success");
        }else{
            
            

            
//            NSString *droptable=@"DROP TABLE IF EXISTS event";
//            [[SQLLiteUtil shared]createTable:droptable];
            
            
            NSString *eventSql=@"CREATE TABLE  IF NOT EXISTS event (id text PRIMARY KEY, userId text,userName text,maxLimit number,level text,startTime datetime,endTime datetime,locationId text,isPrivate text,remark text,type text);";
            
            NSLog(@"success");
            [[SQLLiteUtil shared]createTable:eventSql];
            //取出球場資訊
            NSString *result = [dict objectForKey:@"event"];
            
            NSLog(@"%@",result);
            
            
            //Convert Json to NSArray
            NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding]options:0 error:NULL];
            
            NSLog(@"jsonObject=%@", jsonObject);

            for ( NSDictionary *loca in jsonObject )
            {
                                NSLog(@"----");
                                 NSLog(@"ID: %@", loca[@"ID"] );
                                NSLog(@"MAX_LIMIT: %@", loca[@"MAX_LIMIT"] );
                                 NSLog(@"USER_ID: %@", loca[@"USER_ID"] );
                                NSLog(@"USER_NAME: %@", loca[@"USER_NAME"] );
                                NSLog(@"LOCATION_ID: %@", loca[@"LOCATION_ID"] );
                                NSLog(@"LEVEL: %@", loca[@"LEVEL"] );
                                NSLog(@"IS_PRIVATE: %@", loca[@"IS_PRIVATE"] );
                                NSLog(@"START_TIME: %@", loca[@"START_TIME"] );
                                NSLog(@"END_TIME: %@", loca[@"END_TIME"] );
                                NSLog(@"REMARK: %@", loca[@"REAMRK"] );
                                //NSLog(@"TYPE: %@", loca[@"TYPE"] );
                               NSLog(@"MEMBERS: %@", loca[@"MEMBERS"] );
                                NSLog(@"----");
                
                NSMutableDictionary *insert_Event=[[NSMutableDictionary alloc] init];
                [insert_Event setObject:loca[@"ID"] forKey:@"id"];
                [insert_Event setObject:loca[@"MAX_LIMIT"]  forKey:@"maxLimit"];
                [insert_Event setObject:loca[@"USER_ID"] forKey:@"userId"];
                [insert_Event setObject:loca[@"USER_NAME"]  forKey:@"userName"];
                [insert_Event setObject:loca[@"LOCATION_ID"] forKey:@"locationId"];
                [insert_Event setObject:loca[@"LEVEL"] forKey:@"level"];
                [insert_Event setObject:loca[@"IS_PRIVATE"] forKey:@"isPrivate"];
                [insert_Event setObject:loca[@"START_TIME"] forKey:@"startTime"];
                [insert_Event setObject:loca[@"END_TIME"] forKey:@"endTime"];
                [insert_Event setObject:loca[@"REMARK"] forKey:@"remark"];
                if([[loca objectForKey:@"TYPE"] length]>0){
//                if([[loca objectForKey:@"TYPE" ] length]>0){
                    [insert_Event setObject:loca[@"TYPE"] forKey:@"type"];
                }else{
                     [insert_Event setObject:@"" forKey:@"type"];
                }
                NSString *sql = @"insert or replace  into event (id , userId,userName,maxLimit,level,startTime,endTime,locationId,isPrivate,remark,type) values (:id,:userId,:userName,:maxLimit,:level,:startTime,:endTime,:locationId,:isPrivate,:remark,:type)";
                [[SQLLiteUtil shared]insertDataToDB:sql withDic:insert_Event];
                
            }
        
        }
        
        
        
    }else{
        NSLog(@"Remove Object");
        //               [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"location_time"];
    }
//          NSString *sql  = @"select * from  event where id='EVT1412082382462'";
//        [[SQLLiteUtil shared]selectDataFromDB:sql];
    
    
}
- (NSString *) getUserInfo :(NSString *) userId useToken:(NSString *) token {
    
    gosportsAppDelegate *appDelegate = (gosportsAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSDictionary *_propertyInfo=appDelegate.propertyInfo;
    
    NSMutableDictionary *query_Event=[[NSMutableDictionary alloc] init];
    [query_Event setObject:userId forKey:@"userId"];
    [query_Event setObject:token forKey:@"token"];
    
    NSString *para=[[GetData shared] encodeFormPostParametersWithJson:query_Event];
    // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
    NSLog(@"para:%@ ",para);
    
    NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"user_info_path"]];
    
    NSLog(@"web_url:%@ ",web_url);
    
    NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
    
    NSLog(@"response: %@ ",response);
    return response;
    
    
}




- (NSString *) getEventList :(NSString *) eventId userId:(NSString *) userId withLevel:(NSString *) level{
    
    gosportsAppDelegate *appDelegate = (gosportsAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSDictionary *_propertyInfo=appDelegate.propertyInfo;
    
    NSMutableDictionary *query_Event=[[NSMutableDictionary alloc] init];
    [query_Event setObject:userId forKey:@"userId"];
    [query_Event setObject:level forKey:@"level"];
    [query_Event setObject:eventId forKey:@"eventId"];
    [query_Event setObject:@"" forKey:@"locationId"];
    [query_Event setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [query_Event setObject:[NSNumber numberWithInt:10] forKey:@"row"];
    [query_Event setObject:@"" forKey:@"token"];
    
    NSString *para=[[GetData shared] encodeFormPostParametersWithJson:query_Event];
    // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
    NSLog(@"para:%@ ",para);
    
    NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"query_event_path"]];
    
    NSLog(@"web_url:%@ ",web_url);
    
    NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
    
    NSLog(@"response: %@ ",response);
    return response;

    
}



- (void) registerToken{
         //[[NSUserDefaults standardUserDefaults] setObject: _pushToken forKey:@"pushToken"];

        gosportsAppDelegate *appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
        NSDictionary *_propertyInfo=appDelegate.propertyInfo;
    
    
        NSString *push_token=[[NSUserDefaults standardUserDefaults] stringForKey:@"pushToken"];
    //檢查用戶是否有取道ＴＯＫＥＮ
    if([push_token length]>0){
        NSMutableDictionary *query_Event=[[NSMutableDictionary alloc] init];
        [query_Event setObject:appDelegate.userId forKey:@"userId"];
        [query_Event setObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"pushToken"] forKey:@"pushToken"];
        [query_Event setObject:@"ios" forKey:@"platform"];
        [query_Event setObject:@"" forKey:@"token"];
        
        NSString *para=[[GetData shared] encodeFormPostParametersWithJson:query_Event];
        // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
        NSLog(@"para:%@ ",para);
        
        NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"register_push_path"]];
        
        NSLog(@"web_url:%@ ",web_url);
        
        NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
        
        NSLog(@"response: %@ ",response);
    }else{
        NSLog(@"Token is nil Break!!! ");
    }
    
    
    
}


//compate time 1 and time2 wich is old
-(BOOL) compareTime :(NSString *) dateformat currentTime:(NSString *) time1 compareTime:(NSString *) time2{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateformat];
    
    NSDate *date1= [formatter dateFromString:time1];
    NSDate *date2 = [formatter dateFromString:time2];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        NSLog(@"date1 is later than date2");
        return NO;
    }
    else if(result == NSOrderedAscending)
    {
        NSLog(@"date2 is later than date1");
        return NO;
    }
    else
    {
         NSLog(@"date2 is equal than date1");
        return YES;
    }
    return nil;
    
}



-(NSString *)getShowEventMsg:(NSString *) id withLocationName:(NSString *)locationName{
    //    //取得場地ＮＡＭE
    //取得活動相關訊息
    NSString *sql=@"select * from  event where ID=:id";
    NSDictionary *nameDictionary = [NSDictionary dictionaryWithObjectsAndKeys:id,@"id", nil];
    NSArray *eventarray = [[NSArray alloc] initWithObjects:@"remark",@"locationId",@"userName",@"startTime",@"type" ,nil];
    
    NSDictionary *dic=[[SQLLiteUtil shared]selectDicDataFromDB:sql useKey:eventarray withDic:nameDictionary];
    NSString *showDate=[[ToolUtil shared]changeDateFormt:@"YYYY-MM-dd HH:mm:ss.S" newformat:@"MM/dd HH:mm" dateString:dic[@"startTime"]];
    return [NSString stringWithFormat:@"是否參加由%@ 開始時間:%@ 地點:%@ 發起的活動?", dic[@"userName"],showDate,locationName];

}

-(NSString *)getLocationName:(NSString *) id{
    //    //取得場地ＮＡＭE
    NSString *seqSql=@"select name from  location where seq=:seq";
    NSDictionary *nameDictionary = [NSDictionary dictionaryWithObjectsAndKeys:id,@"seq", nil];
    NSArray *arr=[[SQLLiteUtil shared]selectArrayDataFromDB:seqSql useKey:@"name" withDic:nameDictionary];
    return [arr objectAtIndex:0];
}
-(NSString *)joinEvent:(NSString *) eventId{
    
    gosportsAppDelegate *appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
    NSDictionary *_propertyInfo=appDelegate.propertyInfo;
    
    NSMutableDictionary *create_event_dir=[[NSMutableDictionary alloc] init];
    [create_event_dir setObject:appDelegate.userId forKey:@"userId"];
    [create_event_dir setObject:eventId forKey:@"eventId"];
    [create_event_dir setObject:@"" forKey:@"token"];
    
    NSString *para=[[GetData shared] encodeFormPostParametersWithJson:create_event_dir];
    
    NSLog(@"para:%@ ",para);
    
    NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"join_event_path"]];
    
    NSLog(@"web_url:%@ ",web_url);
    
    NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
    return response;
}
-(NSInteger *)numberPushEvent:(NSString *) method{
    NSInteger number=0;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"pushNumber"]!=nil){
        number=[[NSUserDefaults standardUserDefaults] integerForKey:@"pushNumber"];
    }

    
    if([method isEqual:@"add"]){
        number++;
    }else{
        number--;
    }
    return &number;
}

@end;