//
//  editUserViewController.h
//  TennisLife
//
//  Created by Stanley Liu on 10/31/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface editUserViewController : UIViewController<UIAlertViewDelegate>

@end
