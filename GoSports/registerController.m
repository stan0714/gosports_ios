//
//  registerController.m
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "registerController.h"
#import "GetData.h"
#import "gosportsAppDelegate.h"
#import "ToolUtil.h"
#import "mainViewController.h"

@interface registerController (){
    mainViewController *mainViewController;
    BOOL success;
    gosportsAppDelegate *appDelegate;

}


@end

@implementation registerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    success=false;
    _pwdField.secureTextEntry=YES;
    _confirmpwdField.secureTextEntry=YES;
    appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
}


- (IBAction)regiserAction:(id)sender{
    

    
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    
    [activityView startAnimating];
    
    [self.view addSubview:activityView];

    NSDictionary *_propertyInfo=appDelegate.propertyInfo;

    NSString *account=_accountFiled.text;
     NSString *pwd=_pwdField.text;
    NSString *c_pwd=_confirmpwdField.text;
    NSString *message;
    //檢查欄位是否為空
    if(([account length]==0)||([pwd length]==0)|| ([c_pwd length]==0))
    {
        message=@"請檢查欄位資訊 不可為空";
        NSLog(@"Somt filed is empty");
    }else{
    
    
    if([pwd isEqualToString:c_pwd]){
        
        if([pwd length]>5){
        
        NSMutableDictionary *register_dir=[[NSMutableDictionary alloc] init];
        [register_dir setObject:account forKey:@"user_id"];
        [register_dir setObject:[[ToolUtil shared] MD5: pwd] forKey:@"pwd"];
        [register_dir setObject:[[ToolUtil shared] getNowTime] forKey:@"time"];
        [register_dir setObject:@"" forKey:@"val"];
        
        NSString *para=[[GetData shared] encodeFormPostParametersWithJson:register_dir];
        // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
        NSLog(@"para:%@ ",para);
        
        NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"register_path"]];
        
        NSLog(@"web_url:%@ ",web_url);
        
        NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
        
       NSLog(@"response: %@ ",response);
        
        NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
        
        if(![dict[@"status"] isEqualToString:@"-100" ]){
            message=dict[@"status_desc"];
        }else{
            //Record User name and ID
            appDelegate.userName=account;
            appDelegate.userId=account;
            message=@"成功註冊";
            success=true;
            
        }
        }else{
             message=@"密碼長度不符合規定(需大等於六碼以上)";
        }
        
    }else{
        message=@"密碼確認失敗";
        }
    }
    
    
    [activityView stopAnimating];
    
    if(success){
        [self changetoMainView];
    }else{
    
    UIAlertView *messageUI = [[UIAlertView alloc] initWithTitle:@"訊息通知"
                                                      message:message
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
    [messageUI show];
    }
    

    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)changetoMainView{
    mainViewController *mainviewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    [self presentViewController:mainviewController animated:YES completion:nil];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    success=nil;
    _pwdField=nil;
    _confirmpwdField=nil;
}

@end
