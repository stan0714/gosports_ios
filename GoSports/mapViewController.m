//
//  mapViewController.m
//  TennisLife
//
//  Created by Stanley Liu on 10/16/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "mapViewController.h"
#import "Annotation.h"
#import "SQLLiteUtil.h"

@interface mapViewController ()

@end

@implementation mapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.map.delegate = self;
    self.map.mapType = MKMapTypeStandard;
    self.map.showsUserLocation = YES;


    [self initMap];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) initMap{
    
    
    //取出所有場地資訊
    NSString *seqSql=@"select * from  location";
    
    //NSString *citySql=@"select distinct(city) from  location";
    
    NSArray *arrState=[[SQLLiteUtil shared]selectArrayDataFromDB:seqSql useKey:@"seq"];

    for (NSString *seq in arrState) {
    
        //取出所有場地資訊
        NSString *locationSql=@"select * from  location where seq=:seq ";

        NSDictionary *nameDictionary = [NSDictionary dictionaryWithObjectsAndKeys:seq,@"seq", nil];
        NSArray *eventarray = [[NSArray alloc] initWithObjects:@"latitude",@"longitude",@"name",@"address",nil];
        
        NSDictionary *dic=[[SQLLiteUtil shared]selectDicDataFromDB:locationSql useKey:eventarray withDic:nameDictionary];
    
        // 建立一個CLLocationCoordinate2D
        CLLocationCoordinate2D mylocation;
        mylocation.latitude = [dic[@"latitude"] doubleValue];
        mylocation.longitude =  [dic[@"longitude"] doubleValue];
    
        // 建立一個region，待會要設定給MapView
        MKCoordinateRegion location_digital;
    
        //    // 設定經緯度
        location_digital.center = mylocation;
    
        // 設定縮放比例
        location_digital.span.latitudeDelta = 0.003;
        location_digital.span.longitudeDelta = 0.003;
    
        // 準備一個annotation
    
        Annotation *ann = [[Annotation alloc] initWithCoordinate:mylocation];
        ann.title = dic[@"name"];
        ann.subtitle = dic[@"address"];
    
        [_map setRegion:location_digital];
    
        // 把annotation加進MapView裡
        [_map addAnnotation:ann];
    }
    
    //加入現在位置
    
    NSDictionary *locationDic=(NSDictionary *)[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"nowLocation"];
    NSLog(@"location:%lu",(unsigned long)[locationDic count]);
    if([locationDic count]>0){
    // 建立一個CLLocationCoordinate2D
    CLLocationCoordinate2D mylocation;
    mylocation.latitude = [locationDic[@"latitude"] doubleValue];
    mylocation.longitude =  [locationDic[@"longitude"] doubleValue];
    
    // 建立一個region，待會要設定給MapView
    MKCoordinateRegion location_digital;
    
    //    // 設定經緯度
    location_digital.center = mylocation;
    
    // 設定縮放比例
    location_digital.span.latitudeDelta = 0.003;
    location_digital.span.longitudeDelta = 0.003;
    
//    // 準備一個annotation
//    
//    Annotation *ann = [[Annotation alloc] initWithCoordinate:mylocation];
//    ann.title = @"現在位置";
//    ann.subtitle =@"address";
    
    [_map setRegion:location_digital];
    
//    // 把annotation加進MapView裡
//    [_map addAnnotation:ann];
    }
    
    
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{//按软键盘右下角的搜索按钮时触发
    NSString *searchTerm=[searchBar text];
    //读取被输入的关键字
    [self handleSearchForTerm:searchTerm];
    //根据关键字，进行处理
    //[_search resignFirstResponder];
    //隐藏软键盘
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{//搜索条输入文字修改时触发
    if([searchText length]==0)
    {//如果无文字输入
//        [self resetSearch];
//        [_table reloadData];
        return;
    }
    
    [self handleSearchForTerm:searchText];
    //有文字输入就把关键字传给handleSearchForTerm处理
}

-(void)handleSearchForTerm:(NSString *)searchTerm
{//处理搜索
    NSMutableArray *sectionToRemove=[NSMutableArray new];
    //分组待删除列表
    //[self resetSearch];
    //先重置
    for(NSString *key in _mutableKeys)
    {//循环读取所有的数组
        NSMutableArray *array=[_mutableNames valueForKey:key];
        NSMutableArray *toRemove=[NSMutableArray new];
        //待删除列表
        for(NSString *name in array)
        {//数组内的元素循环对比
            if([name rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location==NSNotFound)
            {
                //rangeOfString方法是返回NSRange对象(包含位置索引和长度信息)
                //NSCaseInsensitiveSearch是忽略大小写
                //这里的代码会在name中找不到searchTerm时执行
                [toRemove addObject:name];
                //找不到，把name添加到待删除列表
            }
        }
        if ([array count]==[toRemove count]) {
            [sectionToRemove addObject:key];
            //如果待删除的总数和数组元素总数相同，把该分组的key加入待删除列表，即不显示该分组
        }
        [array removeObjectsInArray:toRemove];
        //删除数组待删除元素
    }
    [_mutableKeys removeObjectsInArray:sectionToRemove];
    //能过待删除的key数组删除数组
   // [_table reloadData];
    //重载数据
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{//取消按钮被按下时触发
    //[self resetSearch];
    //重置
    searchBar.text=@"";
    //输入框清空
    //[_table reloadData];
    //[_search resignFirstResponder];
    //重新载入数据，隐藏软键盘
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    _map=nil;
    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
