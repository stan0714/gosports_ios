//
//  LocationObject.h
//  GoSports
//
//  Created by Stanley Liu on 9/24/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface LocationObject : NSObject
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *town;
@property (nonatomic, strong) NSNumber *lat;
@end


