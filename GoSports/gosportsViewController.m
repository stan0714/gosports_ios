//
//  gosportsViewController.m
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "gosportsViewController.h"
#import "registerController.h"
#import "loginViewController.h"
#import "SQLLiteUtil.h"
#import "ToolUtil.h"


@interface gosportsViewController (){
    registerController *registerController;
    loginViewController *loginViewController;
}

@end

@implementation gosportsViewController

- (void)viewDidLoad
{
     [super viewDidLoad];
    [[SQLLiteUtil shared]initDB];
    [[ToolUtil shared] getLocationInfo];
//    _tabBar.delegate=self;

    
//    UITabBarItem *loginBarItem = [[UITabBarItem alloc] initWithTitle:@"登入"
//                                                             image:[UIImage imageNamed:@"login_icon"]
//                                                               tag:1];
//    
//    UITabBarItem *registerBarItem = [[UITabBarItem alloc] initWithTitle:@"註冊"
//                                                             image:[UIImage imageNamed:@"register_icon"]
//                                                               tag:0];
    
    //NSMutableArray *itemsArray = [NSArray arrayWithItems:tabBarItem, tabBarItem2, nil];
   
//    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
//    [itemsArray addObject:loginBarItem];
//    [itemsArray addObject:registerBarItem];
//
//    
//    [_tabBar setItems:itemsArray animated:YES];
//    [[self LoginBarItem] setTitle:@"登入"];
//   // self.LoginBarItem.title=@"登入";
//
//    self.RegisterBarItem.title=@"註冊";
    [self.LoginButton setBackgroundImage:[UIImage imageNamed:@"login-green.png"] forState:UIControlStateNormal];
    [self.SignInButton setBackgroundImage:[UIImage imageNamed:@"signup-button.png"] forState:UIControlStateNormal];

}


- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    

}
- (IBAction)changeToLoginPage:(id)sender {
    loginViewController *loginViewcontroller =
    [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self presentViewController:loginViewcontroller animated:YES completion:nil];
}

- (IBAction)changeToRegisterPage:(id)sender {
    registerController *registercontroller =
    [self.storyboard instantiateViewControllerWithIdentifier:@"registerController"];
    [self presentViewController:registercontroller animated:YES completion:nil];
}

//



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

@end
