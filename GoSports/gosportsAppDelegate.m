//
//  gosportsAppDelegate.m
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "gosportsAppDelegate.h"
#import "SimpleKeychain.h"
#import "FMDatabase.h"
#import "ToolUtil.h"
#import "eventJoinViewController.h"

@implementation gosportsAppDelegate{
    NSString *eventId;
    eventJoinViewController *joinViewController;
}


@synthesize pushToken=_pushToken;
@synthesize deviceID=_deviceID;
@synthesize propertyInfo=_propertyInfo;
@synthesize fbuserInfo=_fbuserInfo;
@synthesize userName=_userName;
@synthesize userId=_userId;
@synthesize items=_items;
@synthesize cityArray=_cityArray;
@synthesize eventMd5=_eventMd5;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
     gosportsAppDelegate *appDelegate = (gosportsAppDelegate *)[[UIApplication sharedApplication]delegate];
    //init value from FBDemo plist
    NSString *pilst_path = [[NSBundle mainBundle] pathForResource: @"GoSports" ofType: @"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile: pilst_path];
    NSLog(@"dict %@",dict);
    appDelegate.propertyInfo=dict;
    
    //取得Ｕser Token
    NSLog(@"Registering for push notifications...");
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
         gosportsAppDelegate *appDelegate = (gosportsAppDelegate *)[[UIApplication sharedApplication]delegate];
    
//    [FBSettings setDefaultAppID:appDelegate.propertyInfo[@"facebook_app_id"]];
    [FBAppEvents activateApp];
    
    [FBAppEvents logEvent:appDelegate.propertyInfo[@"app_version"]];
    NSLog(@"Facebook Event Done");
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    // FBSample logic
    // We need to properly handle activation of the application with regards to SSO
    //  (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self.session close];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//FOR APPLE APNS
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    /* Get device token */
    _pushToken = [NSString stringWithFormat:@"%@", deviceToken];
    
    /* Replace '<', '>' and ' ' */
    NSCharacterSet *charDummy = [NSCharacterSet characterSetWithCharactersInString:@"<> "];
    _pushToken = [[_pushToken componentsSeparatedByCharactersInSet: charDummy] componentsJoinedByString: @""];
    _deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSLog(@"Device token=[%@] idfv=[%@]", _pushToken,_deviceID);
    [[NSUserDefaults standardUserDefaults] setObject: _deviceID forKey:@"deviceID"];
    [[NSUserDefaults standardUserDefaults] setObject: _pushToken forKey:@"pushToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    /* 可以把token傳到server，之後server就可以靠它送推播給使用者了 */
}



- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Error=[%@]", err);
    // TODO: when user do not allow push notification service, pop the warning message.
}

// This function called when receive notification and app is in the foreground.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    /* 把收到的推播列舉出來 */
    for (id key in userInfo) {
        NSLog(@"Key=[%@], Value=[%@], userInfo=[%@]", key, [userInfo objectForKey:key],userInfo);
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:*[[ToolUtil shared] numberPushEvent:@"add"]];
        
    }
    
    
//    [[NSUserDefaults standardUserDefaults] setObject: locationDic forKey:@"nowLocation"];
    
    /* 印出 Badge number */
    NSLog(@"Badge: %@", [[userInfo objectForKey:@"aps"] objectForKey:@"badge"]);
    NSLog(@" 收到推送消息 ： %@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
    
//    NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
   
    eventId=[userInfo objectForKey:@"eventId"];
    
    //[UIApplication sharedApplication].applicationIconBadgeNumber ++;
    
    if ([[userInfo objectForKey:@"aps"] objectForKey:@"alert"]!=nil) {
        
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"推送通知"
                              
    message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]
                              
                              
                                                       delegate:self
                              
                              
                                              cancelButtonTitle:@" 關閉"
                              
                              
                                              otherButtonTitles:@" 更新狀態",nil];
        
        
        alert.tag=0;

        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:*[[ToolUtil shared] numberPushEvent:@"sub"]];
        [alert show];
    }

    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"clickButtonAtIndex:%ld",(long)buttonIndex);
    [UIApplication sharedApplication].applicationIconBadgeNumber --;
    if ((long)buttonIndex==1) {
        NSLog(@"Event ID:%@",eventId);
        self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"eventJoinViewController"];
//        eventJoinViewController *notificationViewController = [[eventJoinViewController alloc] init];
//        [self.window.rootViewController presentViewController:notificationViewController animated:YES completion:NULL];
    }

    

}

// FBSample logic
// The native facebook application transitions back to an authenticating application when the user
// chooses to either log in, or cancel. The url passed to this method contains the token in the
// case of a successful login. By passing the url to the handleOpenURL method of FBAppCall the provided
// session object can parse the URL, and capture the token for use by the rest of the authenticating
// application; the return value of handleOpenURL indicates whether or not the URL was handled by the
// session object, and does not reflect whether or not the login was successful; the session object's
// state, as well as its arguments passed to the state completion handler indicate whether the login
// was successful; note that if the session is nil or closed when handleOpenURL is called, the expression
// will be boolean NO, meaning the URL was not handled by the authenticating application
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:self.session];
}


@end
