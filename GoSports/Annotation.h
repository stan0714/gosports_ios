//
//  NSObject+Annotation.h
//  TennisLife
//
//  Created by Stanley Liu on 10/6/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>
@interface Annotation:NSObject <MKAnnotation>

{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

-(id) initWithCoordinate: (CLLocationCoordinate2D) the_coordinate;
@end
