//
//  NSObject+SQLLiteUtil.m
//  GoSports
//
//  Created by Stanley Liu on 9/12/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//
#import "SQLLiteUtil.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@implementation SQLLiteUtil{
    
}


static SQLLiteUtil *_shared = nil;
+ (SQLLiteUtil *)shared {
	if (_shared == nil) {
		_shared = [self new];
	}
	return _shared;
}


-(void)initDB{
//                NSString *create=@"CREATE TABLE  IF NOT EXISTS location (seq number PRIMARY KEY, name text,address text,city text,town text,location text,clay number,grass number,latitude number,longitude number,indoor number,hard number);CREATE TABLE  IF NOT EXISTS event (id text PRIMARY KEY, userId text,userName text,maxLimit number,level text,startTime datetime,endTime datetime,locationId text,isPrivate text)";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    //如果已經建立過就跳過
    if([fileManager fileExistsAtPath:dbPath]) return;
//    //打開通道
//    FMDatabase *db = [FMDatabase databaseWithPath:dbPath] ;
//    if (![db open]) {
//        NSLog(@"Could not open db.");
//        return ;
//    }
//    [db executeUpdate:create];
//    //資料傳完了，通道要關好
//    [db close];
}

- (void)createDatabaseAndTablesIfNeeded{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    //如果已經建立過就跳過
    if([fileManager fileExistsAtPath:dbPath]) return;
    //打開通道
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath] ;
    if (![db open]) {
        NSLog(@"Could not open db.");
        return ;
    }
    [db executeUpdate:@"CREATE TABLE Message (_id integer primary key, message text, time timestamp, read_status)"];
    //資料傳完了，通道要關好
    [db close];
}

- (void)createTableIfNeeded:(NSString *) sql{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    //如果已經建立過就跳過
    if([fileManager fileExistsAtPath:dbPath]) return;
    //打開通道
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath] ;
    if (![db open]) {
        NSLog(@"Could not open db.");
        return ;
    }
    [db executeUpdate:sql];
    //資料傳完了，通道要關好
    [db close];
}


- (void)createTable:(NSString *) sql{
    //NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    //打開通道
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath] ;
    if (![db open]) {
        NSLog(@"Could not open db.");
        return ;
    }
    [db executeUpdate:sql];
    //資料傳完了，通道要關好
    [db close];
}








+ (void) insertDataToDB:(NSString *)message{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    //開啟與資料庫的通道
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        [db executeUpdate:@"INSERT INTO Message(message, time, read_status) VALUES(?,datetime(), ?)", message, @"0"];
    }
    //資料傳完了，通道要關好
    [db close];
}

- (void) insertDataToDB :(NSString *)message withDic:(NSDictionary *)dictionary{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    //開啟與資料庫的通道
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        [db executeUpdate:message withParameterDictionary:dictionary];
         //NSLog(@" db update Success.");

    }
    //資料傳完了，通道要關好
    [db close];
}

- (void) selectDataFromDB:(NSString *)message{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];

    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        FMResultSet *rs = [db executeQuery:message];

        while ([rs next]) {
    
    NSString *name = [rs stringForColumn:@"userName"];
    NSString *longitude = [rs stringForColumn:@"userId"];
    NSLog(@"%@, %@",name,longitude);
    
    }
    [rs close];
    }
    [db close];
}



- (NSMutableArray *) selectArrayDataFromDB:(NSString *)message useKey:(NSString *)key withDic: (NSDictionary *)dictionary{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        
        FMResultSet *rs = [db executeQuery:message withParameterDictionary:dictionary];
        
        while ([rs next]) {
            
            NSString *value = [rs stringForColumn:key];
            
            NSLog(@"%@",value);
             [array addObject:value];
            
        }
        [rs close];
    }
    [db close];
    return array;
    
}

- (NSMutableArray *) selectArrayDataFromDB:(NSString *)message useKey:(NSString *)key{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        
        FMResultSet *rs = [db executeQuery:message];
        
        while ([rs next]) {
            
            NSString *value = [rs stringForColumn:key];
            
            NSLog(@"selectArrayDataFromDB %@",value);
            [array addObject:value];
            
        }
        [rs close];
    }
    [db close];
    return array;
    
}



- (NSDictionary *) selectDicDataFromDB:(NSString *)message useKey:(NSArray *)keys{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        
        FMResultSet *rs = [db executeQuery:message];
        
        while ([rs next]) {
            
            for (NSString *key in keys) {
                NSString *value=[rs stringForColumn:key];
                [dic setObject:value forKey:key];
            }

            
        }
        [rs close];
    }
    [db close];
    return dic;
    
}


- (NSDictionary *) selectDicDataFromDB:(NSString *)message useKey:(NSArray *)keys withDic: (NSDictionary *)dictionary{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"goSports.db"];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    
    if (![db open]) {
        NSLog(@"Could not open db.");
    }else {
        
                FMResultSet *rs = [db executeQuery:message withParameterDictionary:dictionary];
        
        while ([rs next]) {
            
            for (NSString *key in keys) {
                NSString *value=[rs stringForColumn:key];
                if([value length]>0){
                //NSString *value=[rs stringForColumn:key];
                [dic setObject:value forKey:key];
                }else{
                    [dic setObject:@"" forKey:key];
                }
            }
            
            
        }
        [rs close];
    }
    [db close];
    return dic;
    
}


@end
