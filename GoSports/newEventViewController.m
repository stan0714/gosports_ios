//
//  newEventViewController.m
//  GoSports
//
//  Created by Stanley Liu on 9/17/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "newEventViewController.h"
#import "ToolUtil.h"
#import "SQLLiteUtil.h"
#import "gosportsAppDelegate.h"
#import "GetData.h"
#import "mainViewController.h"
#import "eventDetailController.h"

@interface newEventViewController (){
    UIDatePicker *datePicker,*endDataPicker;
    NSLocale *datelocale;
      NSArray *array;
    NSArray *arrState,*typeArray;
    UIPickerView *ntrPicker,*cityPicker,*locationPicker,*typePicker ;
    UIToolbar *mypickerToolbar;
    NSArray *cityArray,*locationArray;
    UIAlertView * alert;
    gosportsAppDelegate *appDelegate;
    mainViewController *mainViewController;
    BOOL success;
    NSString *message;
    UIActivityIndicatorView *spinner;
}

@end

@implementation newEventViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (gosportsAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //取得LOCATION清單
    [[ToolUtil shared] getLocationInfo];
    
    //將結束時間與地點設為不可編輯
    [self.locationText setEnabled:NO];
    [self.endTimeFeild setEnabled:NO];

    
    // 建立 UITextField
    [self initDataPick];
    [self initArray];
    [self initcityPick];
    [self initntrPick];
    [self inittypePick];
    [_remark setText:@"點擊後編輯"];

    self.remark.delegate=self;
    [self initSpinner];

    
}

-(void)initSpinner{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(160, 240);
    spinner.hidden=YES;
    spinner.hidesWhenStopped = YES;
    [self.view bringSubviewToFront:spinner];
    [self.view addSubview:spinner];
}


-(void) initArray{
    
    if (appDelegate.cityArray == nil || [appDelegate.cityArray count] == 0) {
        NSString *citySql=@"select distinct(city) from  location";
        
        arrState=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"city"];
        appDelegate.cityArray =arrState;
        
    }else{
         arrState=appDelegate.cityArray;
    }


    
    array = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9" ,nil];
    //cityArray = [[NSArray alloc] initWithObjects:@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19" ,nil];
    locationArray = [[NSArray alloc] initWithObjects:@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29" ,nil];
    typeArray=[[NSArray alloc] initWithObjects:@"練球",@"競賽" ,nil];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    //NSLog(@"textViewShouldBeginEditing:");
    
    alert =[[UIAlertView alloc ] initWithTitle:@"備註" message:@"點擊中間欄位填入備註內容" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    alert.alertViewStyle =  UIAlertViewStylePlainTextInput;
    alert.delegate=self;
    [alert addButtonWithTitle:@"OK"];
    [alert show];
    
    return YES;
}




- (void)textViewDidBeginEditing:(UITextView *)textView {
    //NSLog(@"textViewDidBeginEditing:");
    //textView.backgroundColor = [UIColor greenColor];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    //NSLog(@"textViewShouldEndEditing:");
    //textView.backgroundColor = [UIColor whiteColor];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    //NSLog(@"textViewDidEndEditing:");
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSString *name = [alertView textFieldAtIndex:0].text;
        //NSLog(@"%@",name);
        [_remark setText:name];
        // Insert whatever needs to be done with "name"
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //NSLog(@"touchesBegan:withEvent:");

    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}






-(void) initntrPick{
    
    self.ntrText.text=@"請選擇等級";
//    arrState = [[NSArray alloc] initWithObjects:@"NewYork",@"NewJercy",@"Carlifornia",@"Florida",@"Fremont",@"SantaClara",@"San Diego",@"San Fransisco",@"San Jose",nil];
    
    
    ntrPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    ntrPicker.delegate = self;
    
    ntrPicker.dataSource = self;
    
    ntrPicker.tag=3;

    
    [ntrPicker  setShowsSelectionIndicator:YES];
    
    self.ntrText.inputView =  ntrPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(ntrPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.ntrText.inputAccessoryView = mypickerToolbar;
    
}


-(void) initcityPick{
    
    self.cityText.text=@"請選擇城市";

     cityPicker= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    cityPicker.delegate = self;
    
    cityPicker.dataSource = self;
    
    cityPicker.tag=4;
    
    [cityPicker  setShowsSelectionIndicator:YES];
    
    self.cityText.inputView =  cityPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(cityPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.cityText.inputAccessoryView = mypickerToolbar;
    
}



-(void) inittypePick{
    
    self.typeText.text=@"請選擇類型";
    //    arrState = [[NSArray alloc] initWithObjects:@"NewYork",@"NewJercy",@"Carlifornia",@"Florida",@"Fremont",@"SantaClara",@"San Diego",@"San Fransisco",@"San Jose",nil];
    
    
    typePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    typePicker.delegate = self;
    
    typePicker.dataSource = self;
    
    typePicker.tag=5;
    
    
    [typePicker  setShowsSelectionIndicator:YES];
    
    self.typeText.inputView =  typePicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(typePickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.typeText.inputAccessoryView = mypickerToolbar;
    
}

-(void)cityPickerDoneClicked

{
    
    //NSLog(@"Done Clicked");
    if ([self.view endEditing:NO]) {
            NSString *stateSelect = [arrState objectAtIndex:[cityPicker selectedRowInComponent:0]];


        self.cityText.text = stateSelect;
        
        NSString *citySql=@"select name from  location where city=:city";
        
        NSDictionary *locDictionary = [NSDictionary dictionaryWithObjectsAndKeys:stateSelect,@"city", nil];
        locationArray=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"name" withDic:locDictionary];
        [self.locationText setEnabled:YES];
        [self initlocationPick];
        
    }
    
}


-(void) initlocationPick{
    
    self.locationText.text=[locationArray objectAtIndex:0];
    //    arrState = [[NSArray alloc] initWithObjects:@"NewYork",@"NewJercy",@"Carlifornia",@"Florida",@"Fremont",@"SantaClara",@"San Diego",@"San Fransisco",@"San Jose",nil];
    
    
    locationPicker= [[UIPickerView alloc] initWithFrame:CGRectMake(0, 43, 320, 480)];
    
    locationPicker.delegate = self;
    
    locationPicker.dataSource = self;
    
    locationPicker.tag=2;
    
    [locationPicker  setShowsSelectionIndicator:YES];
    
    self.locationText.inputView =  locationPicker  ;
    
    
    // 建立 UIToolbar
    mypickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(locationPickerDoneClicked)];
    // 把按鈕加進 UIToolbar
    mypickerToolbar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.locationText.inputAccessoryView = mypickerToolbar;
    
}

-(void)locationPickerDoneClicked

{
    
    //NSLog(@"Location Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [locationArray objectAtIndex:[locationPicker selectedRowInComponent:0]];
        
        
        self.locationText.text = stateSelect;
    }
    
}


-(void)typePickerDoneClicked

{
    
   // NSLog(@"Type Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [typeArray objectAtIndex:[typePicker selectedRowInComponent:0]];
        
        
        self.typeText.text = stateSelect;
    }
    
}


-(void)ntrPickerDoneClicked

{
    
    //NSLog(@"NTR Done Clicked");
    if ([self.view endEditing:NO]) {
        NSString *stateSelect = [array objectAtIndex:[ntrPicker selectedRowInComponent:0]];
        
        
        self.ntrText.text = stateSelect;
    }
    
}









-(void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.ntrText.inputView;
    self.ntrText.text = [NSString stringWithFormat:@"%@",picker.date];
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    //NSLog(@"pickerView numberOfRowsInComponent pickerView.tag %ld",(long)pickerView.tag);
    
    if (pickerView.tag == 3){
        return [array count];
    }
    else if(pickerView.tag == 1){
        return [cityArray count];
    }else if (pickerView.tag==2){
        return [locationArray count];
    }else if (pickerView
              .tag==4){
        return [arrState count];
    }else{
        return [typeArray count];

    }
    
    //return [array count];
}
-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSLog(@"pickerView titleForRow pickerView.tag %ld",(long)pickerView.tag);
    if (pickerView.tag == 3){
        return [array objectAtIndex:row];
    }
    else if(pickerView.tag == 1){
        return [cityArray objectAtIndex:row];
    }else if (pickerView.tag==2){
        return [locationArray objectAtIndex:row];

    }else if(pickerView.tag==4){
        return [ arrState objectAtIndex:row];

    }else{
        return [ typeArray objectAtIndex:row];
    }
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    NSLog(@"pickerView didSelectRow pickerView.tag %ld",(long)pickerView.tag);
    
    if (pickerView.tag == 3){
                NSString *ntv  = [array objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"ntv: %@",ntv);
    }
    else if(pickerView.tag == 1){
                       NSString *city  = [cityArray objectAtIndex:[pickerView selectedRowInComponent:0]];
         NSLog(@"city: %@",city);
    }else if(pickerView.tag == 2){
        NSString *location  = [locationArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"location: %@",location);
    }else if(pickerView.tag==4){
        NSString *arrStat   = [arrState objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"arrState: %@",arrStat);
    }else{
        NSString *typeStat   = [typeArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        NSLog(@"type: %@",typeStat);
    }
    
}


-(void) initDataPick{
    // 以下兩行是測試用 可以依照自己的需求增減屬性
    self.timeFeild.text = @"請選擇";
    // 記得要設定 delegate
    self.timeFeild.delegate = self;
    // 加入 view 中

    
    // 建立 UIDatePicker
    datePicker = [[UIDatePicker alloc]init];
    //設定時區
    datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    [datePicker setCalendar:[NSCalendar currentCalendar]];
    [datePicker setLocale:datelocale];
    //設定間隔
    datePicker.minuteInterval = 15;
    datePicker.timeZone = [NSTimeZone localTimeZone];
    
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    // 以下這行是重點 (螢光筆畫兩行) 將 UITextField 的 inputView 設定成 UIDatePicker
    
    NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setLenient:YES];
    [formatter1 setDateStyle:NSDateFormatterShortStyle];
    [formatter1 setTimeStyle:NSDateFormatterShortStyle];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components weekday] - 30))];
    NSDate *maxDate  = [cal dateFromComponents:components];
    
    
    
    datePicker.minimumDate= [NSDate date];
    datePicker.maximumDate=maxDate;
    
    // 則原本會跳出鍵盤的地方 就改成選日期了
    self.timeFeild.inputView = datePicker;
    
    // 建立 UIToolbar
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self
                                                                          action:@selector(cancelPicker)];
    // 把按鈕加進 UIToolbar
    toolBar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.timeFeild.inputAccessoryView = toolBar;
    

}



-(void) initEndDataPick{
    
    [self.endTimeFeild setEnabled:YES];
    // 以下兩行是測試用 可以依照自己的需求增減屬性
    self.endTimeFeild.text = @"請選擇";
    // 記得要設定 delegate
    self.endTimeFeild.delegate = self;
    // 加入 view 中
    
    
    // 建立 UIDatePicker
    endDataPicker = [[UIDatePicker alloc]init];
    //設定時區
    datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    [endDataPicker setCalendar:[NSCalendar currentCalendar]];
    [endDataPicker setLocale:datelocale];
    //設定間隔
    endDataPicker.minuteInterval = 15;
    endDataPicker.timeZone = [NSTimeZone localTimeZone];
    
    endDataPicker.datePickerMode = UIDatePickerModeDateAndTime;
    // 以下這行是重點 (螢光筆畫兩行) 將 UITextField 的 inputView 設定成 UIDatePicker
    
    NSDateFormatter* formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setLenient:YES];
    [formatter1 setDateStyle:NSDateFormatterShortStyle];
    [formatter1 setTimeStyle:NSDateFormatterShortStyle];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components weekday] - 30))];
    NSDate *maxDate  = [cal dateFromComponents:components];
    
    
    
    endDataPicker.minimumDate= [NSDate date];
    endDataPicker.maximumDate=maxDate;
    
    // 則原本會跳出鍵盤的地方 就改成選日期了
    self.endTimeFeild.inputView = endDataPicker;
    
    // 建立 UIToolbar
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    // 選取日期完成鈕 並給他一個 selector
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self
                                                                          action:@selector(cancelTimePicker)];
    // 把按鈕加進 UIToolbar
    toolBar.items = [NSArray arrayWithObject:right];
    // 以下這行也是重點 (螢光筆畫兩行)
    // 原本應該是鍵盤上方附帶內容的區塊 改成一個 UIToolbar 並加上完成鈕
    self.endTimeFeild.inputAccessoryView = toolBar;
    
    
}



// 按下完成鈕後的 method
-(void) cancelPicker {
    // endEditing: 是結束編輯狀態的 method
    if ([self.view endEditing:NO]) {
        // 以下幾行是測試用 可以依照自己的需求增減屬性
        //NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        // Set the locale as needed in the formatter (this example uses Japanese)
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]
                               ];
        [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        self.timeFeild.text = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:datePicker.date]];
        [self initEndDataPick];
        
    }
}

-(void) cancelTimePicker {
    // endEditing: 是結束編輯狀態的 method
    if ([self.view endEditing:NO]) {
        // 以下幾行是測試用 可以依照自己的需求增減屬性
        //NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        // Set the locale as needed in the formatter (this example uses Japanese)
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]
         ];
        [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        self.endTimeFeild.text = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:endDataPicker.date]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}




- (IBAction)submitAction:(id)sender {
    
    
    BOOL gate=YES;
    NSString *startTime=_timeFeild.text;
    NSString *endTime=_endTimeFeild.text;
    
    //比較開始時間與現在
    
    
    
    
    NSRange range = [startTime rangeOfString:@"選擇"];
//    //檢查地址
//    NSLog(@"位置：%lu || 字串相同長度：%lu", (unsigned long)range.location, (unsigned long)range.length);
//    range = [_ntrText.text rangeOfString:@"選擇"];
//     NSLog(@"NTR：%lu || 字串相同長度：%lu", (unsigned long)range.location, (unsigned long)range.length);

    if([_ntrText.text rangeOfString:@"選擇"].length!=0){
        NSLog(@"不合法的NTR");

    }else{
        NSLog(@"NTR OK");
    }
    
    if([startTime isEqualToString:@"請選擇"]||[endTime isEqualToString:@"請選擇"] ||[_ntrText.text isEqualToString:@"請選擇等級"]){
         NSLog(@"不合法的時間");
        gate=NO;
    }
    

    NSString *isPrivate=@"N";
    if ([_privat isOn]) {
        isPrivate=@"Y";
       // NSLog(@" _privat its on!");
    }
    
    
    NSString *city=_cityText.text;
     NSString *location=_locationText.text;
   NSLog(@"location  %@ %@",city,location);
    
    if([location length]==0||[endTime length]==0||!gate){
        message=@"請檢查參數";
        [self showMessage:message];
        NSLog(@"不合法的參數");
    }else{
    
    
    //取得類型
        NSString *type=_typeText.text;
        
    //取得場地ＩＤ
    NSString *seqSql=@"select seq from  location where city=:city and name=:name";
    
    NSDictionary *locDictionary = [NSDictionary dictionaryWithObjectsAndKeys:city,@"city",location,@"name", nil];
    NSArray *arr=locationArray=[[SQLLiteUtil shared]selectArrayDataFromDB:seqSql useKey:@"seq" withDic:locDictionary];
        NSString *locatonId=[arr objectAtIndex:0];;
    NSLog(@"seq :%@",locatonId);
        NSString *remark_w=_remark.text;
        //NSString
        
        
        NSDictionary *propertyInfo=appDelegate.propertyInfo;
        NSMutableDictionary *create_Event=[[NSMutableDictionary alloc] init];
        [create_Event setObject:appDelegate.userId forKey:@"userId"];
        [create_Event setObject:_ntrText.text forKey:@"level"];
        [create_Event setObject:locatonId forKey:@"locationId"];
        [create_Event setObject:startTime forKey:@"startTime"];
        [create_Event setObject:endTime forKey:@"endTime"];
//        [create_Event setObject:[NSString stringWithFormat:@"%@%@",startTime,@":00"] forKey:@"startTime"];
//        [create_Event setObject:[NSString stringWithFormat:@"%@%@",endTime,@":00"] forKey:@"endTime"];
        [create_Event setObject:type forKey:@"type"];
        [create_Event setObject:remark_w forKey:@"remark"];
        [create_Event setObject:@"2" forKey:@"max"];
        [create_Event setObject:isPrivate forKey:@"isPrivate"];
        
        [create_Event setObject:@"" forKey:@"token"];
        NSString *para=[[GetData shared] encodeFormPostParametersWithJson:create_Event];
        
        NSLog(@"para:%@ ",para);
        
        NSString* web_url=[propertyInfo[@"root_url"] stringByAppendingString:propertyInfo[@"create_event_path"]];
        
        NSLog(@"web_url:%@ ",web_url);
        
        NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
        
        NSLog(@"response: %@ ",response);
        
        NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];

        
        if(![dict[@"status"] isEqualToString:@"-100" ]){
            message=dict[@"status_desc"];
            
        }else{
            success=true;

            message=@"登入成功";
            
        }
    
    //[activityView stopAnimating];
    
    if(success){
        [self changetoMainView];
    }else{
        
        [self showMessage:message];
        
    }
    
    }

}



-(void) showMessage:(NSString *)msg
{
    UIAlertView *messageUI = [[UIAlertView alloc] initWithTitle:@"訊息通知"
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    spinner.hidden=YES;
    [spinner stopAnimating];
    [messageUI show];
}


-(void)changetoMainView{
    mainViewController *mainviewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    [self presentViewController:mainviewController animated:YES completion:nil];
    
}





- (void)viewDidUnload
{
    [super viewDidUnload];

    datePicker=nil;
    endDataPicker=nil;
    datelocale=nil;
    array=nil;
    arrState=nil;
    typeArray=nil;
    ntrPicker=nil;
    cityPicker=nil;
    locationPicker=nil;
    typePicker=nil;
    mypickerToolbar=nil;
    cityArray=nil;
    locationArray=nil;
    alert=nil;
    appDelegate=nil;
    _timeFeild=nil;
    _endTimeFeild=nil;
    _ntrText=nil;
    _cityText=nil;
    _locationText=nil;
    _remark=nil;
    _typeText=nil;
    _privat=nil;
    _submitButton=nil;
}

@end
