//
//  NSObject+SQLLiteUtil.h
//  GoSports
//
//  Created by Stanley Liu on 9/12/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface SQLLiteUtil : NSObject

+ (SQLLiteUtil *)shared;


- (void)createDatabaseAndTablesIfNeeded;
- (void)initDB;
- (void)createTableIfNeeded:(NSString *) sql;
- (void)createTable:(NSString *) sql;

+ (void) insertDataToDB:(NSString *)message;
- (void) insertDataToDB :(NSString *)message withDic:(NSDictionary *)dictionary;
- (void) selectDataFromDB:(NSString *)message;
- (NSMutableArray *) selectArrayDataFromDB:(NSString *)message useKey:(NSString *)key withDic: (NSDictionary *)dictionary;

- (NSMutableArray *) selectArrayDataFromDB:(NSString *)message useKey:(NSString *)key;
- (NSDictionary *) selectDicDataFromDB:(NSString *)message useKey:(NSArray *)key;

- (NSDictionary *) selectDicDataFromDB:(NSString *)message useKey:(NSArray *)keys withDic: (NSDictionary *)dictionary;

@end
