//
//  eventDetailController.m
//  TennisLife
//
//  Created by Stanley Liu on 10/4/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "eventDetailController.h"
#import <QuartzCore/QuartzCore.h>
#import "SQLLiteUtil.h"
#import "ToolUtil.h"
#import "locationMapViewController.h"

@interface eventDetailController (){
    NSDictionary *dic;
    NSString *message;
}

@end

@implementation eventDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.idTextField.text=_string;
    NSArray *locationPic= [[NSArray alloc] initWithObjects:@"hard1.jpg",@"ball1.jpeg",@"ball2.jpeg",@"hard2.jpeg",@"hard3.jpeg",@"hard4.jpeg",@"hard5.jpeg",@"red1.jpeg",@"red2.jpeg",@"room.jpeg" ,nil];

    int value = arc4random() % ([locationPic count]);
    
     _userImage.image= [UIImage imageNamed:@"AppIcon76x76"];
    NSLog(@"Stanley %@ %d %@",_string,value,[locationPic objectAtIndex:value]);
    
    _backgroundImage.image=[UIImage imageNamed:[locationPic objectAtIndex:value]];
    
    //CALayer *imageLayer = _userImage.layer;
    
    _userImage.image= [UIImage imageNamed:@"AppIcon76x76"];
    _userImage.layer.cornerRadius=_userImage.bounds.size.width / 2;
    _userImage.layer.masksToBounds=YES;
    
    _idTextField.hidden=YES;
    [self initView];
    
    // Do any additional setup after loading the view.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"id: %@",[segue identifier]);
    NSString *sid=[segue identifier];
    if([sid isEqualToString:@"showEventDetail"]||[sid isEqualToString:@"BackDetail"]){
    //將page2設定成Storyboard Segue的目標UIViewController
        id page2 = segue.destinationViewController;
    
        //將值透過Storyboard Segue帶給頁面2的string變數
        [page2 setValue:_idTextField.text forKey:@"string"];
    }else if([segue.identifier isEqualToString:@"showMap"]){

        locationMapViewController *destViewController = segue.destinationViewController;
        destViewController.string =_string ;
        destViewController.laction=dic[@"locationId"];

    }

    
    
}
-(void) initView{
    NSLog(@"id:  %@",_idTextField.text);
    NSString *sql=@"select * from  event where ID=:id";
    NSDictionary *nameDictionary = [NSDictionary dictionaryWithObjectsAndKeys:_idTextField.text,@"id", nil];
    NSArray *eventarray = [[NSArray alloc] initWithObjects:@"remark",@"locationId",@"userName",@"startTime",@"type" ,nil];
    dic=[[SQLLiteUtil shared]selectDicDataFromDB:sql useKey:eventarray withDic:nameDictionary];
    NSLog(@"remark  %@",dic[@"remark"]);
    NSLog(@"dic  %@",dic);
//    _userIDTextField.text=dic[@"userName"];
//    _timeTextField.text=[[ToolUtil shared]changeDateFormt:@"YYYY-MM-dd HH:mm:ss.S" newformat:@"MM/dd HH:mm" dateString:dic[@"startTime"]];
    _remarkTextField.text=dic[@"remark"];
//    _locationTextField.text=[[ToolUtil shared]getLocationName: dic[@"locationId"]];
//    NSLog(@"location %@",[[ToolUtil shared]getLocationName: dic[@"locationId"]]);
    _nameLabel.text=dic[@"userName"];
    _locationLabel.text=[[ToolUtil shared]getLocationName: dic[@"locationId"]];
    _timeLabel.text=[[ToolUtil shared]changeDateFormt:@"YYYY-MM-dd HH:mm:ss.S" newformat:@"MM/dd HH:mm" dateString:dic[@"startTime"]];
    _typeLabel.text=dic[@"type"];
    //將TextField設為不可編輯
    [_locationTextField setEnabled:NO];
    [_userIDTextField setEnabled:NO];
    [_timeTextField setEnabled:NO];
    [_typeTextField setEnabled:NO];
    [_remarkTextField setEditable:NO];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    _idTextField=nil;
    _userImage=nil;
    _userIDTextField=nil;
    _timeTextField=nil;
    _locationTextField=nil;
    _typeTextField=nil;
    _remarkTextField=nil;
    _nameLabel=nil;
    _locationLabel=nil;
    _typeLabel=nil;
    _typeLabel=nil;
    _backgroundImage=nil;
    dic=nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)joinAction:(id)sender {

    
        NSString * msg=[[ToolUtil shared]getShowEventMsg:_string withLocationName:_locationLabel.text];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"參與活動確認"
                                                            message:msg
                                  
                                                           delegate:self
                                                  cancelButtonTitle:@"取消"
                                                  otherButtonTitles:@"確定", nil ];
        alertView.alertViewStyle = UIAlertActionStyleDefault;
    alertView.tag=1;
    alertView.delegate=self;
        [alertView show];
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1){
        NSLog(@"clickButtonAtIndex:%ld",(long)buttonIndex);
        if(buttonIndex==1){
            NSLog(@"要送出邀請");
            NSString *res=[[ToolUtil shared]joinEvent:_string];
            NSLog(@"%@",res);
            NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:res];
            if([res length]>0){
                if(![dict[@"status"] isEqualToString:@"-100" ]){
                    message=dict[@"status_desc"];
                    
                }else{
                    message=dict[@"status_desc"];
                }
            }else{
                message=@"異常情況發生 請稍後再試";
            }
        }
        [self showMessage:message];
    }
}

-(void) showMessage:(NSString *)msg
{
    UIAlertView *messageUI = [[UIAlertView alloc] initWithTitle:@"訊息通知"
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    messageUI.tag=999;
    
    
    [messageUI show];
}@end
