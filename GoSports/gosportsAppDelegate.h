//
//  gosportsAppDelegate.h
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface gosportsAppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>{
    BOOL success;
    NSError *error;
    NSArray *path;
    NSFileManager *fm;
    NSString *doucmentsDirectory;
    NSString *writableDBPath;
}

//FOR FMDB
@property (nonatomic,retain) NSMutableArray* items;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FBSession *session;
@property (strong,nonatomic) NSDictionary * propertyInfo;
@property (strong,nonatomic) NSDictionary * fbuserInfo;

//儲存ＴＯＫＥＮ
@property (strong,nonatomic) NSString * pushToken;
@property (strong,nonatomic) NSString * deviceID;

//儲存城市陣列
@property (strong,nonatomic) NSArray * cityArray;
@property (strong,nonatomic) NSString * userName;
@property (strong,nonatomic) NSString * userId;
//eventMd5
@property (strong,nonatomic) NSString * eventMd5;


@end
