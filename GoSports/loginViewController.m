//
//  LoginViewController.m
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "loginViewController.h"
#import "GetData.h"
#import "gosportsAppDelegate.h"
#import "ToolUtil.h"
#import "mainViewController.h"
#import "MBProgressHUD.h"

@interface loginViewController (){
    mainViewController *mainViewController;
    BOOL success;
    UIActivityIndicatorView *spinner;
    gosportsAppDelegate *appDelegate;
    NSDictionary *_propertyInfo;
}

@end

@implementation loginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    appDelegate = [[UIApplication sharedApplication]delegate];
    _propertyInfo=appDelegate.propertyInfo;
    
    
    //[_backgroudImage setImage:[UIImage imageNamed:@"mainwithnewlogo"]];
    //設定圖片的透明度 值為0.0~1.0(圖片隱藏～完全不隱藏)
    //[_backgroudImage setAlpha:0.2];
    
    success=false;
    _passwordField.secureTextEntry=YES;
    

    [_facebookButton setTitle:@"" forState:UIControlStateNormal];
    [_facebookButton setBackgroundImage:[UIImage imageNamed:@"facebook_64.png"]  forState:UIControlStateNormal];

    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center = CGPointMake(160, 240);
        spinner.hidden=YES;
        spinner.hidesWhenStopped = YES;
        [self.view bringSubviewToFront:spinner];
        [self.view addSubview:spinner];
    

}


- (IBAction)loginAction:(id)sender{
    
    NSLog(@"loginAction Start");

    NSString *account=_accountFiled.text;
    NSString *pwd=_passwordField.text;

    NSString *message;
    //檢查欄位是否為空
    if(([account length]==0)||([pwd length]==0))
    {
        message=@"請檢查欄位資訊 不可為空";
        NSLog(@"Somt filed is empty");
    }else{

                
                NSMutableDictionary *register_dir=[[NSMutableDictionary alloc] init];
                [register_dir setObject:account forKey:@"user_id"];
                [register_dir setObject:[[ToolUtil shared] MD5: pwd] forKey:@"pwd"];
                [register_dir setObject:[[ToolUtil shared] getNowTime] forKey:@"time"];
                [register_dir setObject:@"" forKey:@"val"];
                
                NSString *para=[[GetData shared] encodeFormPostParametersWithJson:register_dir];

                NSLog(@"para:%@ ",para);
                
                NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"login_path"]];
                
                NSLog(@"web_url:%@ ",web_url);
                
                NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
                
                NSLog(@"response: %@ ",response);
                
                NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
                
                if(![dict[@"status"] isEqualToString:@"-100" ]){
                    message=dict[@"status_desc"];
                    
                }else{
                    success=true;
                    //Record USer ID and Account;
                    appDelegate.userName=account;
                    appDelegate.userId=account;
                    message=@"登入成功";
                
                }

            

    }
    
    
    //[activityView stopAnimating];
    
    if(success){
        [self changetoMainView];
    }else{
    
        [self showMessage:message];

    }
    
    
    
    
}



// FBSample logic
// handler for button click, logs sessions in or out
- (IBAction)buttonClickHandler:(id)sender {
    // get the app delegate so that we can access the session property
//    gosportsAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
//     NSString *message;
    
    
    NSLog(@"buttonClickHandler Start");
    
    [appDelegate.session closeAndClearTokenInformation];
    
    NSArray *permissions =     [NSArray arrayWithObjects:@"email",@"user_location",@"user_birthday",@"user_hometown",nil];
    appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                            FBSessionState status,
                                                            NSError *error) {
        if(!error)
        {
            NSLog(@"facebookOpenIdLogin success");
            [self update];
            
            
        }
        else
        {
            NSLog(@"facebookOpenIdLogin failure");
        }
        
    }];

}



-(void) showMessage:(NSString *)message
{
    UIAlertView *messageUI = [[UIAlertView alloc] initWithTitle:@"訊息通知"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    spinner.hidden=YES;
    [spinner stopAnimating];
    [messageUI show];
}



-(void)changetoMainView{
    mainViewController *mainviewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    [self presentViewController:mainviewController animated:YES completion:nil];

}



- (void)update {
   NSLog(@"update Start");

    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    
    hud.labelText=@"登入中請稍後!";

//    spinner.hidden=NO;
//    [spinner startAnimating];

    // get the app delegate, so that we can reference the session property
    //gosportsAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (appDelegate.session.isOpen) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self getUser];
            
           if(![[appDelegate.fbuserInfo objectForKey:@"id"] isKindOfClass:[NSNull class]]){
//            if([appDelegate.fbuserInfo objectForKey:@"id"] !=nil]){
 
                
            NSString *response=[self openIdRegisterToServer];
            if (appDelegate.fbuserInfo != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"dispatch_async");
                    [hud hide:YES];
//                    spinner.hidden=YES;
//                    [spinner stopAnimating];

                    @try
                    {
                    NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
                    
                    if(![dict[@"status"] isEqualToString:@"-100" ]){
                        [self showMessage:dict[@"status_desc"]];
                    }else{
                        [self changetoMainView];
                    }
                    }@catch (NSException * e) {
                        
                        [self showMessage:@"網路異常 請稍後再試!!"];

                    }

                    
                });
            }
           }else{
               [self showMessage:@"Facebook異常 請稍後再試!!"];
           }
        });

        

        
    } 
}


- (void)getUser{


    NSString* webUrl=_propertyInfo[@"facebook_graph_url"];
    NSLog(@"get url = %@ ",webUrl);
    NSString* para=[@"access_token=" stringByAppendingString:appDelegate.session.accessTokenData.accessToken];
    [[GetData shared] getHttp:webUrl parameters:para error:nil];
    NSLog(@"get url = %@ ",[[GetData shared] getHttp:webUrl parameters:para error:nil]);
    NSString *response=[[GetData shared] getHttp:webUrl parameters:para error:nil];
    if(response!=nil){
        //convert rto nsdata
        NSData* response1 = [response dataUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"get url = %@ ",response1);
        NSDictionary * _fbuserinfo=[NSJSONSerialization JSONObjectWithData:response1 options:NSJSONReadingAllowFragments error:nil];
        //gosportsAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
        //save to Delegate
        appDelegate.fbuserInfo=_fbuserinfo;
        appDelegate.userName=_fbuserinfo[@"name"];
        appDelegate.userId=_fbuserinfo[@"id"];
        //NSString* id=_fbuserinfo[@"id"];
        NSLog(@"id=%@ name=%@", _fbuserinfo[@"id"],_fbuserinfo[@"name"]);
    }
    
}

- (NSString *)openIdRegisterToServer{
 
    NSDictionary *dic=appDelegate.fbuserInfo;
//     NSDictionary *_propertyInfo=appDelegate.propertyInfo;
    
    
    NSString *sex=[dic objectForKey:@"gender"];
    
    if([sex isEqualToString:@"male"]){
        sex=@"1";
    }else{
        sex=@"0";
    }
    
    NSLog(@"appDelegate.fbuserInfo:%@",appDelegate.fbuserInfo);
    
    NSMutableDictionary *register_dir=[[NSMutableDictionary alloc] init];
    
    
    [register_dir setObject:[dic objectForKey:@"id"] forKey:@"user_id"];
    [register_dir setObject:@"" forKey:@"pwd"];
    [register_dir setObject:[dic objectForKey:@"name"] forKey:@"user_name"];
    
    
    //BOOL isErrorResponse = [dic containsObject:@"email"];
    
    if ([dic objectForKey:@"email"]){
        [register_dir setObject:[dic objectForKey:@"email"] forKey:@"email"];
    }else{
        [register_dir setObject:@"" forKey:@"email"];
    }
    
    if ([dic objectForKey:@"birthday"]){
        [register_dir setObject:[dic objectForKey:@"birthday"] forKey:@"birthday"];
    }else{
        [register_dir setObject:@"" forKey:@"birthday"];
    }
    
//    [register_dir setObject:[dic objectForKey:@"email"] forKey:@"email"];
//    [register_dir setObject:[dic objectForKey:@"birthday"] forKey:@"birthday"];
    [register_dir setObject:sex forKey:@"sex"];
    [register_dir setObject:[[ToolUtil shared] getNowTime] forKey:@"time"];
    [register_dir setObject:@"" forKey:@"val"];
    
    NSString *para=[[GetData shared] encodeFormPostParametersWithJson:register_dir];
    // NSString *para=[[GetData shared] encodeFormPostParameters:register_dir];
    NSLog(@"para:%@ ",para);
    
    NSString* web_url=[_propertyInfo[@"root_url"] stringByAppendingString:_propertyInfo[@"openid_path"]];
    
    NSLog(@"web_url:%@ ",web_url);
    
    NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
    return response;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload
{

    
    [super viewDidUnload];
    ServerResponse=nil;
    _passwordField=nil;
    _accountFiled=nil;
    _backgroudImage=nil;
}


@end
