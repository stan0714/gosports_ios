//
//  LoginViewController.h
//  GoSports
//
//  Created by Stanley Liu on 8/21/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loginViewController : UIViewController{
    NSString *ServerResponse;
}
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UITextField *accountFiled;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIImageView *backgroudImage;



- (IBAction)loginAction:(id)sender;
- (IBAction)buttonClickHandler:(id)sender;


@end
