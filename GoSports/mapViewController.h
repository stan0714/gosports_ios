//
//  mapViewController.h
//  TennisLife
//
//  Created by Stanley Liu on 10/16/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface mapViewController : UIViewController<MKMapViewDelegate,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
//搜尋關鍵字
@property (strong,nonatomic) NSMutableDictionary *mutableNames;
@property (strong,nonatomic)NSMutableArray *mutableKeys;


@end
