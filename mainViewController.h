//
//  mainViewController.h
//  GoSports
//
//  Created by Stanley Liu on 9/9/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface mainViewController : UIViewController<UITabBarDelegate,CLLocationManagerDelegate,UITableViewDelegate, UITableViewDataSource,UITabBarDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *showTextView;
@property (weak, nonatomic) IBOutlet UITabBar *tabView;
@property (strong, nonatomic) NSArray *list;
@property (weak, nonatomic) IBOutlet UITableView *eventTableView;


@end
