//
//  locationMapViewController.m
//  TennisLife
//
//  Created by Stanley Liu on 10/6/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "locationMapViewController.h"
#import "eventDetailController.h"
#import "Annotation.h"
#import "ToolUtil.h"
#import "SQLLiteUtil.h"

@interface locationMapViewController ()

@end

@implementation locationMapViewController{
    NSDictionary *dic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.map.delegate = self;
    self.map.mapType = MKMapTypeStandard;
    self.map.showsUserLocation = YES;
    
    NSLog(@" locationMapViewController: %@",_string);
    NSLog(@" AAA %@",_laction);
    NSString *sql=@"select * from  location where seq=:seq";

    
//                NSString *locationSql=@"CREATE TABLE  IF NOT EXISTS location (seq number PRIMARY KEY, name text,address text,city text,town text,location text,clay number,grass number,latitude number,longitude number,indoor number,hard number)";
    NSDictionary *nameDictionary = [NSDictionary dictionaryWithObjectsAndKeys:_laction,@"seq", nil];
    NSArray *eventarray = [[NSArray alloc] initWithObjects:@"name",@"address",@"city",@"town",@"location",@"latitude",@"longitude" ,nil];
    dic=[[SQLLiteUtil shared]selectDicDataFromDB:sql useKey:eventarray withDic:nameDictionary];
    NSLog(@"latitude %@", dic[@"latitude"]);
     NSLog(@"longitude %@", dic[@"longitude"]);
    NSLog(@" name %@", dic[@"name"]);
    NSLog(@"location %@", dic[@"location"]);
    
    [self initMap];
    _nameLabel.text=dic[@"name"];
    _addressLabel.text=dic[@"address"];
    _remarkText.text=@"";
    [_remarkText setEditable:NO];
    // Do any additional setup after loading the view.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
       if([[segue identifier] isEqualToString:@"showMap"]){
        //將page2設定成Storyboard Segue的目標UIViewController
        id page2 = segue.destinationViewController;
        
        //將值透過Storyboard Segue帶給頁面2的string變數
        [page2 setValue:_idTextField.text forKey:@"string"];
    }else if ([segue.identifier isEqualToString:@"BackDetail"]) {
        eventDetailController *destViewController = segue.destinationViewController;
        destViewController.string = _string;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) initMap{
    // 建立一個CLLocationCoordinate2D
    CLLocationCoordinate2D mylocation;
    mylocation.latitude = [dic[@"latitude"] doubleValue];
    mylocation.longitude =  [dic[@"longitude"] doubleValue];
    
    // 建立一個region，待會要設定給MapView
    MKCoordinateRegion kaos_digital;
    
    // 設定經緯度
    kaos_digital.center = mylocation;
    
    // 設定縮放比例
    kaos_digital.span.latitudeDelta = 0.003;
    kaos_digital.span.longitudeDelta = 0.003;
    
    // 準備一個annotation

     Annotation *ann = [[Annotation alloc] initWithCoordinate:mylocation];
    ann.title = dic[@"name"];
    ann.subtitle = dic[@"address"];
    
    [_map setRegion:kaos_digital];
    
    // 把annotation加進MapView裡
    [_map addAnnotation:ann];
    

}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        NSLog(@"Clicked Pizza Shop");
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Disclosure Pressed" message:@"Click Cancel to Go Back" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.canShowCallout = YES;
            pinView.image = [UIImage imageNamed:@"iTunesArtwork.png"];
            pinView.calloutOffset = CGPointMake(0, 32);
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinView.rightCalloutAccessoryView = rightButton;
            
            // Add an image to the left callout.
            UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iTunesArtwork.png"]];
            pinView.leftCalloutAccessoryView = iconView;
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
