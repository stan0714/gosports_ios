//
//  mainViewController.m
//  GoSports
//
//  Created by Stanley Liu on 9/9/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "mainViewController.h"
#import "gosportsAppDelegate.h"
#import "ToolUtil.h"
#import "GetData.h"
#import "SQLLiteUtil.h"
#import "eventDetailController.h"
#import "mapViewController.h"
#import "editUserViewController.h"
@interface mainViewController (){
    gosportsAppDelegate *appDelegate;
    //抓取ＧＰＳ資訓
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSArray *roleArray,*detailArray,*locationArray,*eventIDArray;
    NSString *joinEventId,*message;
    mapViewController *mapViewController;

    
}



@end

@implementation mainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     appDelegate =(gosportsAppDelegate *) [[UIApplication sharedApplication]delegate];
   
    //取得event清單
    [[ToolUtil shared] getEventList];
    [[ToolUtil shared] registerToken];
    
    NSLog(@"mainViewController viewDidLoad ");
    NSString *msg=[NSString stringWithFormat:@"%@,%@ %@", @"Hi ", appDelegate.userName,@" Nice to meet you"];
    //NSString *msg=[NSString stringWithFormat:@"%@,%@", @"Hi ", @" Login Success!!"];

    [_showTextView setText:msg];
    [self initTabView];
    
    //GPS init
    locationManager=[[CLLocationManager alloc]init];
   
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    geocoder=[[CLGeocoder alloc]init];
    
    if ([locationManager respondsToSelector:
         @selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.distanceFilter = 60 ;
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//    }
    [locationManager startUpdatingLocation];
    [self initDataView];

    // Do any additional setup after loading the view.
}


-(void)initDataView{
    
    NSString *citySql=@"select * from  event";
    
    roleArray=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"userName"];
    //appDelegate.cityArray =arrState;
     detailArray = [[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"startTime"];
    locationArray=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"locationId"];
    eventIDArray=[[SQLLiteUtil shared]selectArrayDataFromDB:citySql useKey:@"id"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [roleArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //製作可重複利用的表格欄位Cell
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                   reuseIdentifier:CellIdentifier];
       // cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    //設定欄位的內容與類型
    UIButton *addFriendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    addFriendButton.frame = CGRectMake(175.0f, 6.0f, 75.0f, 30.0f);
    addFriendButton.tag=indexPath.row;
    [addFriendButton setTitle:@"Join" forState:UIControlStateNormal];
    cell.textLabel.numberOfLines = 2;

//    //取得場地ＮＡＭE
    NSString *locatonName=[[ToolUtil shared]getLocationName:[locationArray objectAtIndex:indexPath.row]];

    NSString *showDate=[[ToolUtil shared]changeDateFormt:@"YYYY-MM-dd HH:mm:ss.S" newformat:@"MM/dd HH:mm" dateString:[detailArray objectAtIndex:indexPath.row]];
    cell.textLabel.text = [roleArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ @ %@",showDate,locatonName];
    cell.imageView.image = [UIImage imageNamed:@"AppIcon50x50"];
    [addFriendButton addTarget:self
                        action:@selector(JoinEvent:)
              forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:addFriendButton];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return cell;
}

- (IBAction)JoinEvent:(UIButton *)sender
{
//    if (sender.tag == 0)
//    {
    
    NSString *locatonName=[[ToolUtil shared]getLocationName:[locationArray objectAtIndex:sender.tag]];
    
        NSLog(@"JoinEvent %ld",(long)sender.tag);
    //[self showJoinAlert:[eventIDArray objectAtIndex:sender.tag] with];
    [self showJoinAlert:[eventIDArray objectAtIndex:sender.tag] locationName:locatonName];
//    }
//    NSLog(@"JoinEvent %@",sender);
}



-(void) initTabView{
    
    _tabView.delegate=self;
    
    
    UITabBarItem *BarItem1 = [[UITabBarItem alloc] initWithTitle:@"球場"
                                                               image:[UIImage imageNamed:@"login_icon"]
                                                                 tag:0];
    
    UITabBarItem *BarItem2 = [[UITabBarItem alloc] initWithTitle:@"球友"
                                                                  image:[UIImage imageNamed:@"register_icon"]
                                                                    tag:1];
    
    UITabBarItem *BarItem3 = [[UITabBarItem alloc] initWithTitle:@"打球"
                                                               image:[UIImage imageNamed:@"login_icon"]
                                                                 tag:2];
    
    UITabBarItem *BarItem4 = [[UITabBarItem alloc] initWithTitle:@"個人"
                                                                  image:[UIImage imageNamed:@"register_icon"]
                                                                    tag:3];
    
    //NSMutableArray *itemsArray = [NSArray arrayWithItems:tabBarItem, tabBarItem2, nil];
    
    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
    [itemsArray addObject:BarItem1];
    [itemsArray addObject:BarItem2];
    [itemsArray addObject:BarItem3];
    [itemsArray addObject:BarItem4];
    
    [_tabView setItems:itemsArray animated:YES];


    
}

-(void) initMain{
        NSDictionary *propertyInfo=appDelegate.propertyInfo;
        NSMutableDictionary *query_Event=[[NSMutableDictionary alloc] init];
        [query_Event setObject:@"" forKey:@"userId"];
        [query_Event setObject:@"" forKey:@"level"];
        [query_Event setObject:@"" forKey:@"locationId"];
        [query_Event setObject:[NSNumber numberWithInt:1] forKey:@"page"];
        [query_Event setObject:[NSNumber numberWithInt:10] forKey:@"row"];
        [query_Event setObject:@"" forKey:@"token"];
        NSString *para=[[GetData shared] encodeFormPostParametersWithJson:query_Event];
    
        NSLog(@"para:%@ ",para);
    
        NSString* web_url=[propertyInfo[@"root_url"] stringByAppendingString:propertyInfo[@"query_event_path"]];
    
        NSLog(@"web_url:%@ ",web_url);
    
        NSString *response= [[GetData shared]  postHttpWithJson:web_url parameters:para error:nil] ;
    
        NSLog(@"response: %@ ",response);
    
        NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:response];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}


//取得GPS位置
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
   // NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSString *longitude=[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString  *latitude= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        // Reverse Geocoding
        NSLog(@"Resolving the Address");
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
            if (error == nil && [placemarks count] > 0) {
                placemark = [placemarks lastObject];
                NSString  *place = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",placemark.subThoroughfare, placemark.thoroughfare,
                                    placemark.postalCode, placemark.locality,
                                    placemark.administrativeArea,
                                    placemark.country];
                NSLog(@"longitude %@ latitude %@ PLACE %@",longitude,latitude,place);
                NSMutableDictionary *locationDic=[[NSMutableDictionary alloc] init];
                //將相關的位置儲存到NSUserDefaults
                [locationDic setObject:longitude forKey:@"longitude"];
                [locationDic setObject:latitude forKey:@"latitude"];
                NSLog(@"SAVE Location:%@",locationDic);
                [[NSUserDefaults standardUserDefaults] setObject: locationDic forKey:@"nowLocation"];
            } else {
                NSLog(@"%@", error.debugDescription);
            }
        } ];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"click %@",[segue identifier]);
    if ([segue.identifier isEqualToString:@"showEventDetail"]) {
        NSIndexPath *indexPath = [self.eventTableView indexPathForSelectedRow];
        eventDetailController *destViewController = segue.destinationViewController;
        destViewController.string = [eventIDArray objectAtIndex:indexPath.row];
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        UITableViewCell *cell = [self.eventTableView cellForRowAtIndexPath:indexPath];
       // NSLog(@"1234 %@", cell.description);
        [self performSegueWithIdentifier:@"showEventDetail" sender:cell];

}

-(void)showJoinAlert:(NSString *)id locationName:(NSString*) location {
    joinEventId=id;
    NSString * msg=[[ToolUtil shared]getShowEventMsg:id withLocationName:location];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"參與活動確認"
                                                        message:msg
                                                        
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                              otherButtonTitles:@"確定", nil ];
    alertView.alertViewStyle = UIAlertActionStyleDefault;
    alertView.delegate=self;
    alertView.tag=1;
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1){
        NSLog(@"clickButtonAtIndex:%ld",(long)buttonIndex);
        if(buttonIndex==1){
            NSLog(@"要送出邀請");
        NSString *res=[[ToolUtil shared]joinEvent:joinEventId];
        NSLog(@"%@",res);
        NSDictionary *dict=[[ToolUtil shared] getJsonNSDictionary:res];
        if([res length]>0){
        if(![dict[@"status"] isEqualToString:@"-100" ]){
            message=dict[@"status_desc"];
            
        }else{
            message=dict[@"status_desc"];
        }
        }else{
            message=@"異常情況發生 請稍後再試";
        }
    }
        [self showMessage:message];
    }
}

-(void) showMessage:(NSString *)msg
{
    UIAlertView *messageUI = [[UIAlertView alloc] initWithTitle:@"訊息通知"
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    messageUI.tag=999;
    

    [messageUI show];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSLog(@"%ld",(long)item.tag);
    switch (item.tag) {
        case 0:
        {
            NSLog(@"change view");
            mapViewController *view =
            [self.storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
            [self presentViewController:view animated:YES completion:nil];

        }
            break;

        case 1:
        {
            NSLog(@"change view");
//            loginViewController *loginViewcontroller =
//            [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
//            [self presentViewController:loginViewcontroller animated:YES completion:nil];

        }
            break;
            
        case 2:
        {
            NSLog(@"change view");
            //            loginViewController *loginViewcontroller =
            //            [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
            //            [self presentViewController:loginViewcontroller animated:YES completion:nil];
            
        }
            break;
            
        case 3:
        {
            NSLog(@"change view");
            
             editUserViewController *view =
            [self.storyboard instantiateViewControllerWithIdentifier:@"editUserViewController"];
            [self presentViewController:view animated:YES completion:nil];

            
        }
            break;
            
            
        default:
        {

        }

            break;

    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.list = nil;
    self.tabView=nil;
    self.showTextView=nil;
    self.eventTableView=nil;
    joinEventId=nil;
    roleArray=nil;
    detailArray=nil;
    locationArray=nil;
    eventIDArray=nil;    
}


@end
