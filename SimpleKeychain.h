//
//  NSObject+SimpleKeychain.h
//  myplay1_sample_code
//
//  Created by Stanley Liu on 8/20/14.
//  Copyright (c) 2014 TWM. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SimpleKeychainUserPass;

@interface SimpleKeychain : NSObject


+ (SimpleKeychain *)shared;

+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;

@end
