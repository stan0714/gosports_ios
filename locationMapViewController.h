//
//  locationMapViewController.h
//  TennisLife
//
//  Created by Stanley Liu on 10/6/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface locationMapViewController : UIViewController<MKMapViewDelegate>
@property (weak) NSString *string;
@property (weak) NSString *laction;
@property (weak, nonatomic) IBOutlet UITextField *idTextField;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextView *remarkText;

@end
