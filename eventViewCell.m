//
//  eventViewCell.m
//  TennisLife
//
//  Created by Stanley Liu on 10/1/14.
//  Copyright (c) 2014 GoSports. All rights reserved.
//

#import "eventViewCell.h"

@implementation eventViewCell
@synthesize nameLabel = _nameLabel;
@synthesize subLabel =_subLabel;
@synthesize imageView = _imageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
